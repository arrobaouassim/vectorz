package com.erebos.commons.quickhull;

import com.erebos.core.InternalErrorException;
import com.erebos.math.Vec3;

public class Face {
	
	static final int VISIBLE = 1;
	static final int NON_CONVEX = 2;
	static final int DELETED = 3;
	
	int mark = VISIBLE;

	public HalfEdge hedge;
	public Vertex outside;
	public Face next;
	
	public float area;
	public float planeOffset;
	public int index;
	public int numVerts;
	
	private Vec3 normal;
	private Vec3 centroid;
	
	public Face(){ 
		
		normal = new Vec3();
		centroid = new Vec3();
		mark = VISIBLE;
		
	}
	
	public Vec3 computeCentroid(Vec3 centroid){
		
		centroid.setZero();
		HalfEdge he = hedge;
		
		do{
			
			centroid.add(he.head().point);
			he = he.next;
			
		}while(he != hedge);
		
		centroid.scale(1/(float)numVerts);
		
		return centroid;
		
	}
	
	public void computeNormal(Vec3 normal){
		
		HalfEdge he1 = hedge.next;
		HalfEdge he2 = he1.next;
		
		Vec3 p0 = hedge.head().point;
		Vec3 p2 = he1.head().point;
		
		float d2x = p2.x - p0.x;
		float d2y = p2.y - p0.y;
		float d2z = p2.z - p0.z;
		
		normal.setZero();
		
		numVerts = 2;
		
		while(he2 != hedge){
			
			float d1x = d2x;
			float d1y = d2y;
			float d1z = d2z;
			
			p2 = he2.head().point;
			d2x = p2.x - p0.x;
		    d2y = p2.y - p0.y;
		    d2z = p2.z - p0.z;
		    
		    normal.x += d1y*d2z - d1z*d2y;
		    normal.y += d1z*d2x - d1x*d2z;
		    normal.z += d1x*d2y - d1y*d2x;
		    
		    he1 = he2;
		    he2 = he2.next;
		    numVerts++;		
		    		    
		}
		
		area = normal.length();
		normal.scale(1/area);
				
	}
	
	public void computeNormal(Vec3 normal, float minArea){
		
		computeNormal(normal);
		
		if(area < minArea){
			
			// Make the normal more robust by removing components parallel to the longest edge
			
			HalfEdge hedgeMax = null;
			HalfEdge he = hedge;
			float lenSqrMax = 0;
			
			do{
				
				float lenSqr = he.lengthSquared();
				
				if(lenSqr > lenSqrMax){
					
					hedgeMax = he;
					lenSqrMax = lenSqr;
					
				}
				
				he = he.next;
				
			}while(he != hedge);
			
			Vec3 p1 = hedgeMax.tail().point;
			Vec3 p2 = hedgeMax.head().point;
		    float lenMax = (float) Math.sqrt(lenSqrMax);
		    float ux = (p2.x - p1.x)/lenMax;
		    float uy = (p2.y - p1.y)/lenMax;
		    float uz = (p2.z - p1.z)/lenMax;	   
		    float dot = normal.x*ux + normal.y*uy + normal.z*uz;
		    normal.x -= dot*ux;
		    normal.y -= dot*uy;
		    normal.z -= dot*uz;
		      
		    normal.normalize();
		    
		}
						
	}
	
	private void computeNormalAndCentroid(){
		
		computeNormal(normal);
		computeCentroid(centroid);
		
		planeOffset = Vec3.dot(normal, centroid);
		int numv = 0;
		
		HalfEdge he = hedge;
		
		do{
			
			numv++;
			he = he.next;
			
		}while(he != hedge);
		
		if(numv != numVerts){
			
			throw new InternalErrorException("Face " + getVertexString() + " numVerts = " + numVerts + " should be " + numv);
			
		}
		
	}
	
	private void computeNormalAndCentroid(float minArea){
		
		computeNormal(normal, minArea);
		computeCentroid(centroid);
		planeOffset = Vec3.dot(normal, centroid);
		
	}
	
	public static Face createTriangle(Vertex v0, Vertex v1, Vertex v2){
		
		return createTriangle(v0, v1, v2, 0);
	   
	}

	/**
	 * Constructs a triangular Face from vertices v0, v1, and v2.
	 *
	 * @param v0 first vertex
	 * @param v1 second vertex
	 * @param v2 third vertex
	 */	
	public static Face createTriangle(Vertex v0, Vertex v1, Vertex v2, float minArea){
		
		Face face = new Face();
		
		HalfEdge he0 = new HalfEdge(v0, face);
		HalfEdge he1 = new HalfEdge(v1, face);
		HalfEdge he2 = new HalfEdge(v2, face);

		he0.prev = he2;
		he0.next = he1;
		he1.prev = he0;
		he1.next = he2;
		he2.prev = he1;
		he2.next = he0;

		face.hedge = he0;

		// compute the normal and offset
		face.computeNormalAndCentroid(minArea);
		
		return face;
		
	}
	
	public static Face create(Vertex[] vtxArray, int[] indices){
						
		Face face = new Face();
		HalfEdge hePrev = null;
		
		for (int i=0; i<indices.length; i++){
			
			HalfEdge he = new HalfEdge (vtxArray[indices[i]], face); 
			
			if(hePrev != null){
				
				he.setPrev (hePrev);
				hePrev.setNext(he);
				
		    }else{
		    	
		    	face.hedge = he; 
		    	
		    }
			
			hePrev = he;			
			
		}
		
		face.hedge.setPrev(hePrev);
		hePrev.setNext(face.hedge);
	
		// Compute the normal and offset
		face.computeNormalAndCentroid();
		
		return face;	
	   
	}
	
	/**
	 * Gets the i-th half-edge associated with the face.
	 * 
	 * @param i the half-edge index, in the range 0-2.
	 * @return the half-edge
	 */	
	public HalfEdge getEdge(int i){
		
		HalfEdge he = hedge;
		
		while (i > 0){
			
			he = he.next;
			i--;
			
		}
		
		while (i < 0){ 
			
			he = he.prev;		
			i++;
			
	    }
		
		return he;
		
	}

	public HalfEdge getFirstEdge(){ 
		
		return hedge;
		
	}
	
	/**
	 * Finds the half-edge within this face which has
	 * tail <code>vt</code> and head <code>vh</code>.
	 *
	 * @param vt tail point
	 * @param vh head point
	 * @return the half-edge, or null if none is found.
	 */
	public HalfEdge findEdge(Vertex vt, Vertex vh){
		
		HalfEdge he = hedge;
		
		do{
			
			if(he.head() == vh && he.tail() == vt) { return he; }
			
	      he = he.next;
	      
		}
		
	   while (he != hedge);
		
	   return null;
	   
	}
	
	/**
	 * Computes the distance from a point p to the plane of
	 * this face.
	 *
	 * @param p the point
	 * @return distance from the point to the plane
	 */
	public double distanceToPlane(Vec3 p){
		
		return normal.x*p.x + normal.y*p.y + normal.z*p.z - planeOffset;
		
	}
	
	public String getVertexString(){
		
		String s = null;
		HalfEdge he = hedge;
		
		do{
			
			if(s == null){
				
				s = "" + he.head().index;
				
			}else{
				
				s += " " + he.head().index;
				
			}
			
			he = he.next;
			
		}while(he != hedge);
		
		return s;
		
	 }
	
	public void getVertexIndices(int[] idxs){
		
		HalfEdge he = hedge;
		
		int i = 0;
		
		do{
			
			idxs[i++] = he.head().index;
			he = he.next;
			
	    }
		
		while (he != hedge);
		
	}
	
	private Face connectHalfEdges(HalfEdge hedgePrev, HalfEdge he){
		
		Face discardedFace = null;

		if (hedgePrev.oppositeFace() == he.oppositeFace()){ 
			
			// Then there is a redundant edge that we can get rid off
			Face oppFace = he.oppositeFace();
			HalfEdge hedgeOpp;

			if(hedgePrev == hedge){
				
				hedge = he; 
				
			}
			
			if(oppFace.numVerts == 3){
				
				// Then we can get rid of the opposite face altogether
				hedgeOpp = he.opposite.prev.opposite;
				
				oppFace.mark = DELETED;
				discardedFace = oppFace;
			}else{
				
				hedgeOpp = he.opposite.next;
				
				if (oppFace.hedge == hedgeOpp.prev){
					
					oppFace.hedge = hedgeOpp; 
					
				}
				
				hedgeOpp.prev = hedgeOpp.prev.prev;
				hedgeOpp.prev.next = hedgeOpp;
				
			}
			
			he.prev = hedgePrev.prev;
			he.prev.next = he;

			he.opposite = hedgeOpp;
			hedgeOpp.opposite = he;

			// oppFace was modified, so need to recompute
			oppFace.computeNormalAndCentroid();
			
		}else{
			
			hedgePrev.next = he;
			he.prev = hedgePrev;
			
		}
		
		return discardedFace;
		
	}

	public int mergeAdjacentFace(HalfEdge hedgeAdj, Face[] discarded){
		
		Face oppFace = hedgeAdj.oppositeFace();
		int numDiscarded = 0;
		
		discarded[numDiscarded++] = oppFace;
		oppFace.mark = DELETED;
		
		HalfEdge hedgeOpp = hedgeAdj.opposite;

		HalfEdge hedgeAdjPrev = hedgeAdj.prev;
		HalfEdge hedgeAdjNext = hedgeAdj.next;
		HalfEdge hedgeOppPrev = hedgeOpp.prev;
		HalfEdge hedgeOppNext = hedgeOpp.next;
		
		while(hedgeAdjPrev.oppositeFace() == oppFace){
			
			hedgeAdjPrev = hedgeAdjPrev.prev;
			hedgeOppNext = hedgeOppNext.next;
			
		}
		
		while(hedgeAdjNext.oppositeFace() == oppFace){
			
			hedgeOppPrev = hedgeOppPrev.prev;
			hedgeAdjNext = hedgeAdjNext.next;
			
		}
		
		HalfEdge he;
		
		for(he = hedgeOppNext; he != hedgeOppPrev.next; he = he.next){
			
			he.face = this;
			
		}
		
		if(hedgeAdj == hedge){
			
			hedge = hedgeAdjNext;
			
		}
		
		// Handle the half edges at the head
		Face discardedFace;
		
		discardedFace = connectHalfEdges(hedgeOppPrev, hedgeAdjNext);
		
		if(discardedFace != null){
			
			discarded[numDiscarded++] = discardedFace;
			
		}
		
		// Handle the half edges at the tail
		discardedFace = connectHalfEdges(hedgeAdjPrev, hedgeOppNext);
		
		if(discardedFace != null){
			
			discarded[numDiscarded++] = discardedFace;
			
		}
		
		computeNormalAndCentroid();
		checkConsistency();
		
		return numDiscarded;		
		
	}
		
	public void checkConsistency(){
		
		// Do a sanity check on the face
		HalfEdge he = hedge; 
		float maxd = 0;
		int numv = 0;

		if(numVerts < 3){
			
			throw new InternalErrorException("degenerate face: " + getVertexString());
			
		}
		
		do{
			
			HalfEdge hedgeOpp = he.opposite;
			
			if(hedgeOpp == null){
				throw new InternalErrorException("face " + getVertexString() + ": " + "unreflected half edge " + he.getVertexString());
				
			}else if(hedgeOpp.opposite != he){
				throw new InternalErrorException("face " + getVertexString() + ": " + "opposite half edge " + hedgeOpp.getVertexString() +
												 " has opposite " + hedgeOpp.opposite.getVertexString());
			}
			
			if(hedgeOpp.head() != he.tail() || he.head() != hedgeOpp.tail()){
				
				throw new InternalErrorException("face " + getVertexString() + ": " + "half edge " + he.getVertexString() +
												 " reflected by " + hedgeOpp.getVertexString());
				
			}
			
			Face oppFace = hedgeOpp.face;
			
			if(oppFace == null){
				
				throw new InternalErrorException("face " + getVertexString() + ": " + "no face on half edge " + hedgeOpp.getVertexString());
				
			}else if(oppFace.mark == DELETED){
				throw new InternalErrorException("face " + getVertexString() + ": " + "opposite face " + oppFace.getVertexString() + 
												 " not on hull");
				
			}
			
			float d = (float) Math.abs(distanceToPlane(he.head().point));
			
			if(d > maxd) { maxd = d; }
			numv++;
			he = he.next;
			
		}while (he != hedge);

		if(numv != numVerts){
			
			throw new InternalErrorException("face " + getVertexString() + " numVerts=" + numVerts + " should be " + numv);
			
		}

	}
	
	private double areaSquared(HalfEdge hedge0, HalfEdge hedge1){
		
		// Return the squared area of the triangle defined
		// by the half edge hedge0 and the point at the
		// head of hedge1.

		Vec3 p0 = hedge0.tail().point;
		Vec3 p1 = hedge0.head().point;
		Vec3 p2 = hedge1.head().point;

		float dx1 = p1.x - p0.x;
		float dy1 = p1.y - p0.y;
		float dz1 = p1.z - p0.z;

		float dx2 = p2.x - p0.x;
		float dy2 = p2.y - p0.y;
		float dz2 = p2.z - p0.z;

		float x = dy1*dz2 - dz1*dy2;
		float y = dz1*dx2 - dx1*dz2;
		float z = dx1*dy2 - dy1*dx2;

		return x*x + y*y + z*z;	   
	
	}
	
	public void triangulate (FaceList newFaces, float minArea){
		
		HalfEdge he;

		if(numVerts < 4) { return; }

		Vertex v0 = hedge.head();
		Face prevFace = null;

		he = hedge.next;
		HalfEdge oppPrev = he.opposite;
		Face face0 = null;

		for(he=he.next; he!=hedge.prev; he=he.next){
			
			Face face = createTriangle(v0, he.prev.head(), he.head(), minArea);
			
			face.hedge.next.setOpposite(oppPrev);
			face.hedge.prev.setOpposite(he.opposite);
			
			oppPrev = face.hedge;
			newFaces.add (face);
			
			if(face0 == null) { face0 = face; }
			
		}
		
		he = new HalfEdge (hedge.prev.prev.head(), this);
		he.setOpposite (oppPrev);

		he.prev = hedge;
		he.prev.next = he;

		he.next = hedge.prev;
		he.next.prev = he;

		computeNormalAndCentroid(minArea);
		checkConsistency();

		for(Face face=face0; face!=null; face=face.next){
			
			face.checkConsistency(); 
			
		}
		
	}
	
	public Vec3 getNormal() {return normal; }
	
	public Vec3 getCentroid() { return centroid; }
	
}
