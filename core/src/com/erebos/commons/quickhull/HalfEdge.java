package com.erebos.commons.quickhull;

import com.erebos.math.Vec3;

public class HalfEdge {
	
	public Vertex vertex;
	public Face face;
	
	public HalfEdge next, prev, opposite;
	
	public HalfEdge(){
		
	}
	
	public HalfEdge(Vertex vertex, Face face){
		
		this.vertex = vertex;
		this.face = face;
		
	}
	
	public void setNext(HalfEdge edge){
		
		next = edge;
		
	}
	
	public void setPrev(HalfEdge edge){
		
		prev = edge;
		
	}
	
	public void setOpposite(HalfEdge edge){
		
		opposite = edge;
		edge.opposite = this;
		
	}
	
	public Vertex head(){
		
		return vertex;
		
	}
	
	public Vertex tail(){
		
		return prev != null ? prev.vertex : null;
		
	}
	
	public Face oppositeFace(){
		
		return opposite != null ? opposite.face : null;
		
	}
	
	public String getVertexString(){
		
		if(tail() != null){
			
			return "" + tail().index + "-" + head().index;
			
		}else{
			
			return "?-" + head().index;
			
		}
		
	}
	
	public float length(){
		
		if(tail() != null){
			
			return Vec3.distance(head().point, tail().point);
			
		}else{
			
			return -1;				
			
		}
		
	}
	
	public float lengthSquared(){
		
		if(tail() != null){
			
			return Vec3.distanceSquared(head().point, tail().point);
			
		}else{
			
			return -1;				
			
		}
		
	}
	
}
