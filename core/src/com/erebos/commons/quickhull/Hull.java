package com.erebos.commons.quickhull;

import com.badlogic.gdx.utils.Array;
import com.erebos.math.Vec3;
import com.erebos.physics.Shape;

public class Hull extends Shape{
	
	public Array<Vertex> vertices;
	public Array<HalfEdge> edges;
	public Array<Face> faces;
	public Vec3 centroid;
	
	public Hull(Array<Vertex> vertices, Array<Face> faces){
		
		this.vertices = vertices;
		this.faces = faces;		
		init();
		
	}
	
	public void init(){
		
		edges = new Array<HalfEdge>();
		Array<HalfEdge> usededges = new Array<HalfEdge>();
		
		for(Face f : faces){
			
			HalfEdge he = f.hedge;
			
			do{
				
				if(!findEdge(usededges, he)){
					
					edges.add(he);
					edges.add(he.opposite);
					
					usededges.add(he);
					usededges.add(he.opposite);
					
				}			
				
				he = he.next;				
				
			}while(he != f.hedge);
			
		}
		
		usededges.clear();
		
	}
	
	private boolean findEdge(Array<HalfEdge> edges, HalfEdge he){
				
		for(HalfEdge edge : edges){
			
			if(edge.head() == he.head() && edge.tail() == he.tail()) { return true; }
			
		}
				
	   return false;
	   
	}

	public Vec3 getSupportPoint(Vec3 direction){
		
		float bestProjection = -Float.MAX_VALUE;
		Vec3 support = null;
		
		for(Vertex vertex : vertices){
			
			float projection = Vec3.dot(vertex.point, direction);
			
			if(projection > bestProjection){
				
				bestProjection = projection;
				support = new Vec3(vertex.point);
				
			}
			
		}
		
		return support;		
		
	}
}
