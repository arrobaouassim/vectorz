package com.erebos.commons.quickhull;

import java.io.IOException;
import java.io.InputStream;
import java.io.PrintStream;
import java.util.Iterator;
import java.util.Vector;

import com.badlogic.gdx.utils.Array;
import com.erebos.math.Vec3;

public class HullBuilder {
		
	/**
	 * Specifies that (on output) vertex indices for a face should be
	 * listed in clockwise order.
	 */
	public static final int CLOCKWISE = 0x1;

	/**
	 * Specifies that (on output) the vertex indices for a face should be
	 * numbered starting from 1.
	 */
	public static final int INDEXED_FROM_ONE = 0x2;

	/**
	 * Specifies that (on output) the vertex indices for a face should be
	 * numbered starting from 0.
	 */
	public static final int INDEXED_FROM_ZERO = 0x4;

	/**
	 * Specifies that (on output) the vertex indices for a face should be
	 * numbered with respect to the original input points.
	 */
	public static final int POINT_RELATIVE = 0x8;

	/**
	 * Specifies that the distance tolerance should be
	 * computed automatically from the input point data.
	 */
	public static final double AUTOMATIC_TOLERANCE = -1;
	
	/**
	 * Precision of a double.
	 */
	static private final double DOUBLE_PREC = 2.2204460492503131e-16;

	protected int findIndex = -1;

	// Estimated size of the point set
	protected double charLength;

	protected boolean debug = true;

	protected Vertex[] pointBuffer;
	protected int[] vertexPointIndices;
	private Face[] discardedFaces;

	private Vertex[] maxVtxs;
	private Vertex[] minVtxs;

	protected Vector faces;
	protected Vector horizon;

	private FaceList newFaces = new FaceList();
	private VertexList unclaimed = new VertexList();
	private VertexList claimed = new VertexList();

	protected int numVertices;
	protected int numFaces;
	protected int numPoints;

	protected double explicitTolerance = AUTOMATIC_TOLERANCE;
	protected double tolerance;
	
	
	private void init(){
		
		pointBuffer = new Vertex[0];
		vertexPointIndices = new int[0];
		discardedFaces = new Face[3];

		maxVtxs = new Vertex[3];
		minVtxs = new Vertex[3];

		faces = new Vector(16);
		horizon = new Vector(16);

		newFaces = new FaceList();
		unclaimed = new VertexList();
		claimed = new VertexList();
		
	}
	
	private void addPointToFace(Vertex vtx, Face face){
		
		vtx.face = face;
		
		if(face.outside == null){
			
			claimed.add(vtx);
						
		}else{
			
			claimed.insertBefore(vtx, face.outside); 
			
		}
		
		face.outside = vtx;
		
	}
	
	private void removePointFromFace(Vertex vtx, Face face){
		
		if(vtx == face.outside){
			
			if(vtx.next != null && vtx.next.face == face){
				
				face.outside = vtx.next;						
				
			}else{
				
				face.outside = null;
				
			}
			
		}
		
		claimed.delete(vtx); 		
		
	}
	
	private Vertex removeAllPointsFromFace(Face face){
		
		if(face.outside != null){
			
			Vertex end = face.outside;
			
			while(end.next != null && end.next.face == face){
				
				end = end.next;
				
			}
			
			claimed.delete(face.outside, end);
			end.next = null;
			
			return face.outside;
			
		}else{
			
			return null;
			
		}
		
	}
	
	public HullBuilder(){
	}
	
	/**
	 * Creates a convex hull object and initializes it to the convex hull
	 * of a set of points.
	 *
	 * @param points input points.
	 * @throws IllegalArgumentException the number of input points is less
	 * than four, or the points appear to be coincident, colinear, or
	 * coplanar.
	 */
	public HullBuilder(Vec3[] points) throws IllegalArgumentException{
		
		build(points, points.length);
		
	}
	
	private HalfEdge findHalfEdge(Vertex tail, Vertex head){
		
		// Brute force... OK, since setHull is not used much
		for(Iterator it = faces.iterator(); it.hasNext(); ){
			
			HalfEdge he = ((Face)it.next()).findEdge(tail, head);
			
			if(he != null){
				
				return he;
				
			}
			
		}
		
		return null;
		
	}
	
	protected void setHull(Vec3[] coords, int nump, int[][] faceIndices, int numf){
		
		initBuffers(nump);
		setPoints(coords, nump);
		computeMaxAndMin();
		
		for(int i = 0; i > numf; i++){
			
			Face face = Face.create(pointBuffer, faceIndices[i]);
			HalfEdge he = face.hedge;
			
			do{
				
				HalfEdge heOpp = findHalfEdge(he.head(), he.tail());
				
				if(heOpp != null){
					
					he.setOpposite(heOpp);
					
				}
				
				he = he.next;
				
			}while(he != face.hedge);
			
			faces.add(face);
			
		}
		
	}
	
	private void printQhullErrors(Process proc) throws IOException{
		
		boolean wrote = false;
		InputStream es = proc.getErrorStream();
		
		while(es.available() > 0){
			
			System.out.write(es.read());
			wrote = true;
			
		}
		
		if(wrote){
			
			System.out.println("");
			
		}
		
	}
	
	private void printPoints(PrintStream ps){
		
		for(int i = 0; i < numPoints; i++){
			
			Vec3 point = pointBuffer[i].point;
			ps.println(point.x + ", " + point.y + ", " + point.z + ",");
			
		}
		
	}
	
	public Hull build(Vec3[] points) throws IllegalArgumentException{
		
		build(points, points.length);
		
		Array<Vertex> vertices = new Array<Vertex>();
		Array<Face> afaces = new Array<Face>();
		
		for(int i = 0; i < numVertices; i++){
	 		   
			vertices.add(pointBuffer[vertexPointIndices[i]]);
	 		   
	 	}
		
		for(Iterator it=faces.iterator(); it.hasNext(); ){
			
			Face face = (Face)it.next();
			afaces.add(face); 
			
		}
		
		Hull hull = new Hull(vertices, afaces);
		
		return hull;		
		
	}
	
	/**
	 * Constructs the convex hull of a set of points.
	 *
	 * @param points input points
	 * @param nump number of input points
	 * @throws IllegalArgumentException the number of input points is less
	 * than four or greater then the length of <code>points</code>, or the
	 * points appear to be coincident, colinear, or coplanar.
	 */
	public void build(Vec3[] points, int nump) throws IllegalArgumentException{
		
		if(nump < 4){
			
			throw new IllegalArgumentException("Less than four points specified");
			
		}
		
		if(points.length < nump){
			
			throw new IllegalArgumentException("Point array too small for specified number of points");
						
		}
		
		init();
		initBuffers(nump);
		setPoints(points, nump);
		buildHull();
		
	}
	
	/**
	 * Triangulates any non-triangular hull faces. In some cases, due to
	 * precision issues, the resulting triangles may be very thin or small,
	 * and hence appear to be non-convex (this same limitation is present
	 * in <a href=http://www.qhull.org>qhull</a>).
	 */
	public void triangulate(){
		
		double minArea = 1000*charLength*DOUBLE_PREC;
		newFaces.clear();
		
		for(Iterator it = faces.iterator(); it.hasNext(); ){
			
			Face face = (Face)it.next();
			
			if(face.mark == Face.VISIBLE){
				
				face.triangulate(newFaces, (float)minArea);
				// splitFace(face);
				
			}
			
		}
		
		for(Face face = newFaces.first(); face!= null; face = face.next){
			
			faces.add(face);
			
		}
		
	}
	
	protected void initBuffers(int nump){
		
		if(pointBuffer.length < nump){
			
			Vertex[] newBuffer = new Vertex[nump];
			vertexPointIndices = new int[nump];
			
			for(int i = 0; i < pointBuffer.length; i++){
				
				newBuffer[i] = pointBuffer[i];
				
			}
			
			for(int i = pointBuffer.length; i < nump; i++){
				
				newBuffer[i] = new Vertex();
				
			}
			
			pointBuffer = newBuffer;
			
		}
		
		faces.clear();
		claimed.clear();
		numFaces = 0;
		numPoints = nump;
		
	}
	
	protected void setPoints(Vec3[] points, int nump){ 
		
		for(int i=0; i<nump; i++){ 
			
			Vertex vtx = pointBuffer[i];
			vtx.point.set (points[i]);
			vtx.index = i;
			
		}
		
	}

	protected void computeMaxAndMin(){
		
		Vec3 max = new Vec3();
		Vec3 min = new Vec3();
		
		for(int i = 0; i < 3; i++){
			
			maxVtxs[i] = minVtxs[i] = pointBuffer[0];
			
		}
		
		max.set (pointBuffer[0].point);
		min.set (pointBuffer[0].point);
		
		for(int i = 1; i < numPoints; i++){
			
			Vec3 point = pointBuffer[i].point;
			
			if(point.x > max.x){
				
				max.x = point.x;
				maxVtxs[0] = pointBuffer[i];
				
			}else if(point.x < min.x){
				
				min.x = point.x;
				minVtxs[0] = pointBuffer[i];
				
			}
			
			if(point.y > max.y){
				
				max.y = point.y;
				maxVtxs[1] = pointBuffer[i];
				
			}else if(point.y < min.y){
				
				min.y = point.y;
				minVtxs[1] = pointBuffer[i];
				
			}
			
			if(point.z > max.z){
				
				max.z = point.z;
				maxVtxs[2] = pointBuffer[i];
				
		    }else if(point.z < min.z){
		    	
		    	min.z = point.z;
		    	minVtxs[2] = pointBuffer[i];
		    	
		    }
			
		}
		
		// This epsilon formula comes from QuickHull, and I'm
		// not about to quibble.
		
		charLength = Math.max(max.x-min.x, max.y-min.y);
		charLength = Math.max(max.z-min.z, charLength);
		
		if(explicitTolerance == AUTOMATIC_TOLERANCE){
			
			tolerance = 3*DOUBLE_PREC*(Math.max(Math.abs(max.x),Math.abs(min.x))+
									   Math.max(Math.abs(max.y),Math.abs(min.y))+
									   Math.max(Math.abs(max.z),Math.abs(min.z)));
		}else{
			
			tolerance = explicitTolerance; 
		}
		
	}
	
	/**
	 * Creates the initial simplex from which the hull will be built.
	 */	
	protected void createInitialSimplex() throws IllegalArgumentException{
		
		double max = 0;
		int imax = 0;

		for(int i=0; i<3; i++){
			
			double diff = maxVtxs[i].get(i)-minVtxs[i].get(i);
			
			if(diff > max){
				
				max = diff;
				imax = i;
				
			}
			
		}
		
		if(max <= tolerance){
			
			throw new IllegalArgumentException("Input points appear to be cincident");
			
		}
		
		Vertex[] vtx = new Vertex[4];
		
		// Set first two vertices to be those with the greatest
		// one dimensional separation

		vtx[0] = maxVtxs[imax];
		vtx[1] = minVtxs[imax];

		// Set third vertex to be the vertex farthest from
		// the line between vtx0 and vtx1
		
		Vec3 u01 = new Vec3();
		Vec3 diff02 = new Vec3();
		Vec3 nrml = new Vec3();
		Vec3 xprod = new Vec3();
		
		double maxSqr = 0;
		u01 = Vec3.sub(vtx[1].point, vtx[0].point);
		u01.normalize();
		
		for(int i = 0; i < numPoints; i++){
			
			diff02 = Vec3.sub(pointBuffer[i].point, vtx[0].point);
			xprod = Vec3.cross(u01, diff02);
			double lenSqr = xprod.lengthSquared();
			
			if(lenSqr > maxSqr && pointBuffer[i] != vtx[0] && pointBuffer[i] != vtx[1]){
				
				maxSqr = lenSqr;
				vtx[2] = pointBuffer[i];
				nrml.set(xprod);
				
			}
			
		}
		
		if(Math.sqrt(maxSqr) <= 100*tolerance){
			
			throw new IllegalArgumentException("Input points appear to be colinear");
			
		}
		
		nrml.normalize();

		double maxDist = 0;
		double d0 = Vec3.dot(vtx[2].point, nrml);
		
		
		
		for(int i=0; i<numPoints; i++){
			
			double dist = Math.abs (Vec3.dot(pointBuffer[i].point, nrml) - d0);
			if(dist > maxDist && pointBuffer[i] != vtx[0] &&  // paranoid
								 pointBuffer[i] != vtx[1] &&
								 pointBuffer[i] != vtx[2]){
				
				maxDist = dist;
				vtx[3] = pointBuffer[i];
				
			}
	    }
		
		if(Math.abs(maxDist) <= 100*tolerance){
			
			throw new IllegalArgumentException("Input points appear to be coplanar");
			
		}

	  	if(debug){
	  			  		
	  		System.out.println ("initial vertices:");
	  		System.out.println (vtx[0].index + ": " + vtx[0].point.getString());
	  		System.out.println (vtx[1].index + ": " + vtx[1].point.getString());
	  		System.out.println (vtx[2].index + ": " + vtx[2].point.getString());
	  		System.out.println (vtx[3].index + ": " + vtx[3].point.getString());
		   
	  	}
	  	
	  	Face[] tris = new Face[4];

	  	if(Vec3.dot(vtx[3].point, nrml) - d0 < 0){ 
	  		
	  		tris[0] = Face.createTriangle (vtx[0], vtx[1], vtx[2]);
	  		tris[1] = Face.createTriangle (vtx[3], vtx[1], vtx[0]);
	  		tris[2] = Face.createTriangle (vtx[3], vtx[2], vtx[1]);
	  		tris[3] = Face.createTriangle (vtx[3], vtx[0], vtx[2]);

	  		for(int i=0; i<3; i++){
	  			
	  			int k = (i+1)%3;
	  			tris[i+1].getEdge(1).setOpposite (tris[k+1].getEdge(0));
	  			tris[i+1].getEdge(2).setOpposite (tris[0].getEdge(k));
	  			
	  		}
	  			  		
	  	}else{
	  		
	  		tris[0] = Face.createTriangle (vtx[0], vtx[2], vtx[1]);
	  		tris[1] = Face.createTriangle (vtx[3], vtx[0], vtx[1]);
	  		tris[2] = Face.createTriangle (vtx[3], vtx[1], vtx[2]);
	  		tris[3] = Face.createTriangle (vtx[3], vtx[2], vtx[0]);

	  		for (int i=0; i<3; i++){
	  			
	  			int k = (i+1)%3;
	  			tris[i+1].getEdge(0).setOpposite (tris[k+1].getEdge(1));
	  			tris[i+1].getEdge(2).setOpposite (tris[0].getEdge((3-i)%3));
	  			
	  		}
	  		
	  	}
	  	
	  	for(int i=0; i<4; i++){
	  		
	  		faces.add(tris[i]); 
	  		
	  	}
	  	
	  	for(int i=0; i<numPoints; i++){
	  		
	  		Vertex v = pointBuffer[i];

	  		if(v == vtx[0] || v == vtx[1] || v == vtx[2] || v == vtx[3]){
	  			continue;
	  		}

	  		maxDist = tolerance;
	  		Face maxFace = null;
	  		
	  		for(int k=0; k<4; k++){
	  			
	  			double dist = tris[k].distanceToPlane(v.point);
	  			
	  			if(dist > maxDist){
	  				
	  				maxFace = tris[k];
	  				maxDist = dist;
	  			}
	  			
	  		}
	  		
	  		if(maxFace != null){
	  			
	  			addPointToFace (v, maxFace);
	  			
	  		}	
	  		
	  	}
	  	
	}
	
	/**
	 * Returns the vertex points in this hull.
	 *
	 * @return array of vertex points
	 * @see QuickHull3D#getVertices(double[])
	 * @see QuickHull3D#getFaces()
	 */	
 	public Vec3[] getVertices(){
 		
 		Vec3[] vtxs = new Vec3[numVertices];
 		
 		for(int i=0; i<numVertices; i++){
 			
 			vtxs[i] = pointBuffer[vertexPointIndices[i]].point;
 			
 		}
 		
	  	return vtxs;
	}
	
	/**
	 * Returns the coordinates of the vertex points of this hull.
	 *
	 * @param coords returns the x, y, z coordinates of each vertex.
	 * This length of this array must be at least three times
	 * the number of vertices.
	 * @return the number of vertices
	 * @see QuickHull3D#getVertices()
	 * @see QuickHull3D#getFaces()
	 */	
 	public int getVertices(double[] coords){
 		
 	   for(int i=0; i<numVertices; i++){
 		   
 		   Vec3 point = pointBuffer[vertexPointIndices[i]].point;
 		   coords[i*3+0] = point.x;
 		   coords[i*3+1] = point.y;
 		   coords[i*3+2] = point.z;
 		   
 	   }
 	   
	   return numVertices;
	   
	}
	
	/**
	 * Returns an array specifing the index of each hull vertex
	 * with respect to the original input points.
	 *
	 * @return vertex indices with respect to the original points
	 */
	public int[] getVertexPointIndices(){ 
		
		int[] indices = new int[numVertices];
		
		for (int i=0; i<numVertices; i++){
			
			indices[i] = vertexPointIndices[i];
			
		}
		
		return indices;
	   
	}
 	
	/**
	 * Returns the faces associated with this hull.
	 *
	 * <p>Each face is represented by an integer array which gives the
	 * indices of the vertices. These indices are numbered
	 * relative to the
	 * hull vertices, are zero-based,
	 * and are arranged counter-clockwise. More control
	 * over the index format can be obtained using
	 * {@link #getFaces(int) getFaces(indexFlags)}.
	 *
	 * @return array of integer arrays, giving the vertex
	 * indices for each face.
	 * @see QuickHull3D#getVertices()
	 * @see QuickHull3D#getFaces(int)
	 */
	public int[][] getFaces(){
		
		return getFaces(0);
		
	}

	/**
	 * Returns the faces associated with this hull.
	 *
	 * <p>Each face is represented by an integer array which gives the
	 * indices of the vertices. By default, these indices are numbered with
	 * respect to the hull vertices (as opposed to the input points), are
	 * zero-based, and are arranged counter-clockwise. However, this
	 * can be changed by setting {@link #POINT_RELATIVE
	 * POINT_RELATIVE}, {@link #INDEXED_FROM_ONE INDEXED_FROM_ONE}, or
	 * {@link #CLOCKWISE CLOCKWISE} in the indexFlags parameter.
	 *
	 * @param indexFlags specifies index characteristics (0 results
	 * in the default)
	 * @return array of integer arrays, giving the vertex
	 * indices for each face.
	 * @see QuickHull3D#getVertices()
	 */
	public int[][] getFaces(int indexFlags){
		
		int[][] allFaces = new int[faces.size()][];
		int k = 0;
		for(Iterator it=faces.iterator(); it.hasNext(); ){
			
			Face face = (Face)it.next();
			allFaces[k] = new int[face.numVerts];
			getFaceIndices(allFaces[k], face, indexFlags);
			
			k++;
			
		}
		
		return allFaces;
		
	}
	
	/**
	 * Prints the vertices and faces of this hull to the stream ps.
	 *
	 * <p> This is done using the Alias Wavefront .obj file format, with
	 * the vertices printed first (each preceding by the letter
	 * <code>v</code>), followed by the vertex indices for each face (each
	 * preceded by the letter <code>f</code>).
	 *
	 * <p>By default, the face indices are numbered with respect to the
	 * hull vertices (as opposed to the input points), with a lowest index
	 * of 1, and are arranged counter-clockwise. However, this
	 * can be changed by setting {@link #POINT_RELATIVE POINT_RELATIVE},
	 * {@link #INDEXED_FROM_ONE INDEXED_FROM_ZERO}, or {@link #CLOCKWISE
	 * CLOCKWISE} in the indexFlags parameter.
	 *
	 * @param ps stream used for printing
	 * @param indexFlags specifies index characteristics
	 * (0 results in the default).
	 * @see QuickHull3D#getVertices()
	 * @see QuickHull3D#getFaces()
	 */
	public void print(PrintStream ps, int indexFlags){
		
		if((indexFlags & INDEXED_FROM_ZERO) == 0){
			
			indexFlags |= INDEXED_FROM_ONE;
			
	    }
		
		for(int i=0; i<numVertices; i++){
			
			Vec3 pnt = pointBuffer[vertexPointIndices[i]].point;
			ps.println ("v " + pnt.x + " " + pnt.y + " " + pnt.z);
			
	    }
		
		for(Iterator fi=faces.iterator(); fi.hasNext(); ){
			
			Face face = (Face)fi.next();
			int[] indices = new int[face.numVerts];
			getFaceIndices(indices, face, indexFlags);

			ps.print ("f");
			
	      	for(int k=0; k<indices.length; k++){
	      		
	      		ps.print (" " + indices[k]); 
	      		
	      	}
	      	
	      	ps.println ("");
	      	
		}		
		
	}

	private void getFaceIndices(int[] indices, Face face, int flags){ 
		
		boolean ccw = ((flags & CLOCKWISE) == 0);
		boolean indexedFromOne = ((flags & INDEXED_FROM_ONE) != 0);
		boolean pointRelative = ((flags & POINT_RELATIVE) != 0);

		HalfEdge he = face.hedge;
		int k = 0;
		
		do{ 
			
			int idx = he.head().index;
			if(pointRelative){
				
				idx = vertexPointIndices[idx];
				
			}
			if(indexedFromOne){ 
				
				idx++;
				
			}
			
			indices[k++] = idx;
			he = (ccw ? he.next : he.prev);
			
		}while(he != face.hedge);	  
		
	}

	protected void resolveUnclaimedPoints(FaceList newFaces){
		
		Vertex vtxNext = unclaimed.first();
		
		for(Vertex vtx=vtxNext; vtx!=null; vtx=vtxNext){
			
			vtxNext = vtx.next;
	      
			double maxDist = tolerance;
			Face maxFace = null;
			
			for(Face newFace=newFaces.first(); newFace != null; newFace=newFace.next){ 
				
				if(newFace.mark == Face.VISIBLE){ 
					
					double dist = newFace.distanceToPlane(vtx.point);
					
					if (dist > maxDist){
						
						maxDist = dist;
						maxFace = newFace;
						
					}	
					
					if(maxDist > 1000*tolerance) { break; }
				}
				
			}
						
			if(maxFace != null){ 
							 
				addPointToFace (vtx, maxFace);
				 
				if(debug && vtx.index == findIndex){
					
					System.out.println (findIndex + " CLAIMED BY " + maxFace.getVertexString()); 
					
				}
								
			}else{
								 
				if(debug && vtx.index == findIndex){
					 
					System.out.println (findIndex + " DISCARDED");
					 
				} 
				 
			}
						 
		}
				
	}

	protected void deleteFacePoints(Face face, Face absorbingFace){
		
		Vertex faceVtxs = removeAllPointsFromFace (face);
		
		if(faceVtxs != null){ 
			
			if(absorbingFace == null){
				
				unclaimed.addAll (faceVtxs);
				
			}else{
				
				Vertex vtxNext = faceVtxs;
				
				for(Vertex vtx=vtxNext; vtx!=null; vtx=vtxNext){
					vtxNext = vtx.next;
					double dist = absorbingFace.distanceToPlane(vtx.point);
					
					if(dist > tolerance){ 				
						
						addPointToFace(vtx, absorbingFace);
						
					}else{ 
						
						unclaimed.add(vtx);
												
					}
					
				}
				
			}
			
		}
		
	}	

	private static final int NONCONVEX_WRT_LARGER_FACE = 1;
	private static final int NONCONVEX = 2;

	protected double oppFaceDistance (HalfEdge he){
		
	   return he.face.distanceToPlane(he.opposite.face.getCentroid());
	   
	}

	private boolean doAdjacentMerge(Face face, int mergeType){
		
		HalfEdge he = face.hedge;		
		boolean convex = true;
		
		do{ 
			
			Face oppFace = he.oppositeFace();
			boolean merge = false;
			double dist1, dist2;

			if(mergeType == NONCONVEX){
				
				// Then merge faces if they are definitively non-convex
				if(oppFaceDistance (he) > -tolerance || oppFaceDistance (he.opposite) > -tolerance){
					
					merge = true;
				}
			}else{ 
				
				// mergeType == NONCONVEX_WRT_LARGER_FACE
				// merge faces if they are parallel or non-convex
				// wrt to the larger face; otherwise, just mark
				// the face non-convex for the second pass.
				
				if(face.area > oppFace.area){
					
					if((dist1 = oppFaceDistance (he)) > -tolerance){
						
						merge = true;
						
					}else if(oppFaceDistance (he.opposite) > -tolerance){
						
						convex = false;
						
					}
					
				}else{
					
					if(oppFaceDistance (he.opposite) > -tolerance){
						
						merge = true;
						
					}else if(oppFaceDistance (he) > -tolerance){
						
						convex = false;
					}
					
				}	
				
			}

			if(merge){
				
				if(debug){
					
					System.out.println("  merging " + face.getVertexString() + "  and  " + oppFace.getVertexString());
					
				}
				
				int numd = face.mergeAdjacentFace (he, discardedFaces);
				
				for(int i=0; i<numd; i++){
					
					deleteFacePoints(discardedFaces[i], face);
				
				}
				
				if(debug){
					
					System.out.println("  result: " + face.getVertexString());
					
				}
				
				return true;
			}
			
			he = he.next;
			
	    }while (he != face.hedge);
		
		if(!convex){
			
			face.mark = Face.NON_CONVEX; 
			
	    }
		
		return false;
		
	}	

	protected void calculateHorizon(Vec3 eyePnt, HalfEdge edge0, Face face, Vector horizon){
		
		// oldFaces.add (face);
		deleteFacePoints (face, null);
		face.mark = Face.DELETED;
		
		if(debug){
			System.out.println ("  visiting face " + face.getVertexString());
		}
		
		HalfEdge edge;
		
		if(edge0 == null){ 
			
			edge0 = face.getEdge(0);
			edge = edge0;
			
		}else{
			
			edge = edge0.next;
			
		}
		
		do{
			
			Face oppFace = edge.oppositeFace();
			
			if(oppFace.mark == Face.VISIBLE){
				
				if(oppFace.distanceToPlane (eyePnt) > tolerance){
					
					calculateHorizon (eyePnt, edge.opposite, oppFace, horizon);
				}else{
					
					horizon.add (edge);
					
				    if(debug){
				    	
				    	System.out.println ("  adding horizon edge " + edge.getVertexString());
				    	
				    }
				}
			}
			
			edge = edge.next;
			
		}while(edge != edge0);
		
	}
	
	private HalfEdge addAdjoiningFace(Vertex eyeVtx, HalfEdge he){
		
		Face face = Face.createTriangle(eyeVtx, he.tail(), he.head());
		faces.add (face);
		face.getEdge(-1).setOpposite(he.opposite);
		
		return face.getEdge(0);
		
	}

	protected void addNewFaces(FaceList newFaces, Vertex eyeVtx, Vector horizon){ 
		
		newFaces.clear();

		HalfEdge hedgeSidePrev = null;
		HalfEdge hedgeSideBegin = null;

		for(Iterator it=horizon.iterator(); it.hasNext(); ){
			
			HalfEdge horizonHe = (HalfEdge)it.next();
			HalfEdge hedgeSide = addAdjoiningFace (eyeVtx, horizonHe);
			
			if(debug){
				
				System.out.println("new face: " + hedgeSide.face.getVertexString());
				
			}
			
			if(hedgeSidePrev != null){
				
				hedgeSide.next.setOpposite (hedgeSidePrev);		 
			}else{
				
				hedgeSideBegin = hedgeSide; 
				
			}
			
			newFaces.add (hedgeSide.face);
			hedgeSidePrev = hedgeSide;
		}
		
		hedgeSideBegin.next.setOpposite (hedgeSidePrev);
		
	}

	protected Vertex nextPointToAdd(){
				
		if(!claimed.isEmpty()){
			
			Face eyeFace = claimed.first().face;
			Vertex eyeVtx = null;
			double maxDist = 0;
			
			for(Vertex vtx=eyeFace.outside; vtx != null && vtx.face==eyeFace; vtx = vtx.next){
				
				double dist = eyeFace.distanceToPlane(vtx.point);
				
				if(dist > maxDist){ 
					
					maxDist = dist;
					eyeVtx = vtx;
					
				}
			}
			
			return eyeVtx;
			
		}else { return null; }
				
	}
		
	protected void addPointToHull(Vertex eyeVtx){
		
		horizon.clear();
	    unclaimed.clear();
	      
	    if(debug){
	    	
	    	System.out.println("Adding point: " + eyeVtx.index);
	    	System.out.println(" which is " + eyeVtx.face.distanceToPlane(eyeVtx.point) + 
	    					   " above face " + eyeVtx.face.getVertexString());
	    }
	    
	    removePointFromFace (eyeVtx, eyeVtx.face);
	    calculateHorizon (eyeVtx.point, null, eyeVtx.face, horizon);
	    newFaces.clear();
	    addNewFaces (newFaces, eyeVtx, horizon);
	     
	    // First merge pass ... merge faces which are non-convex
	    // as determined by the larger face
	     
	    for(Face face = newFaces.first(); face!=null; face=face.next){ 
	    	
	    	if(face.mark == Face.VISIBLE){
	    		
	    		while(doAdjacentMerge(face, NONCONVEX_WRT_LARGER_FACE));
	    		
	    	}
	    }	
	    
	    // Second merge pass ... merge faces which are non-convex
	    // wrt either face	  
	    
	    for(Face face = newFaces.first(); face!=null; face=face.next){
	    	
	    	if(face.mark == Face.NON_CONVEX){
	    		
	    		face.mark = Face.VISIBLE;
	    		while(doAdjacentMerge(face, NONCONVEX));
	    	}
	    }	
	    
	    resolveUnclaimedPoints(newFaces);
	    
	}

	protected void buildHull(){
		
		int cnt = 0;
		Vertex eyeVtx;

		computeMaxAndMin ();
		createInitialSimplex ();
		
		while((eyeVtx = nextPointToAdd()) != null){
			
			addPointToHull(eyeVtx);
			cnt++;
			
			if(debug){
				
				System.out.println ("iteration " + cnt + " done"); 
			}
			
	    }
		
		reindexFacesAndVertices();
		
		if(debug){
			
			System.out.println("Hull done");
			
	    }
		
	}

	private void markFaceVertices(Face face, int mark){
		
		HalfEdge he0 = face.getFirstEdge();
		HalfEdge he = he0;
		
		do{
			
			he.head().index = mark;
			he = he.next;
			
	    }while(he != he0);
		
	}
	
	protected void reindexFacesAndVertices(){ 
		
		for(int i=0; i<numPoints; i++){
			
			pointBuffer[i].index = -1; 
			
	    }
		
		// Remove inactive faces and mark active vertices
		numFaces = 0;
		
		for(Iterator it=faces.iterator(); it.hasNext(); ){
			
			Face face = (Face)it.next();
			
			if(face.mark != Face.VISIBLE){
				
				it.remove();
				
			}else{
				
				markFaceVertices(face, 0);
				
				numFaces++;
			}
						
		}
		
		// Reindex vertices
		numVertices = 0;
		
		for(int i=0; i<numPoints; i++){
			
			Vertex vtx = pointBuffer[i];
			
			if(vtx.index == 0){
				
				vertexPointIndices[numVertices] = i;
				vtx.index = numVertices++;
				
			}
			
	    }
		
	}

	protected boolean checkFaceConvexity(Face face, double tol, PrintStream ps){
		
		double dist;
		HalfEdge he = face.hedge;
		
		do{
			
			face.checkConsistency();
			
			// Make sure edge is convex
			dist = oppFaceDistance(he);
			
			if(dist > tol){
				
				if(ps != null){
					ps.println ("Edge " + he.getVertexString() + " non-convex by " + dist);
					
				}
				
				return false;
				
			}
			
			dist = oppFaceDistance(he.opposite);
			
			if(dist > tol){
				
				if(ps != null){
					
					ps.println("Opposite edge " + he.opposite.getVertexString() + " non-convex by " + dist);
					
				}
				
				return false;
			}
			
			if(he.next.oppositeFace() == he.oppositeFace()){
				
				if(ps != null){
					
					ps.println ("Redundant vertex " + he.head().index + " in face " + face.getVertexString());
					
				}
				
				return false;
				
			}
			
			he = he.next;
			
		}while (he != face.hedge);	
		
		return true;
				
	}
	
	protected boolean checkFaces(double tol, PrintStream ps){
		
		// Check edge convexity
		
		boolean convex = true;
		
		for(Iterator it=faces.iterator(); it.hasNext(); ){
			
			Face face = (Face)it.next();
			
			if(face.mark == Face.VISIBLE){
				
				if(!checkFaceConvexity(face, tol, ps)){
					
					convex = false;
					
				}
				
			}
			
		}
		
		return convex;
		
	}	
	
}
