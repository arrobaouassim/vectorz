package com.erebos.commons.quickhull;

import com.erebos.math.Vec3;

// TODO: Try to use "double" instead of "float" numbers.

public class Vertex {
	
	public Vec3 point;
	public int index;
	public Vertex next, prev;
	public Face face;
	
	public Vertex(){
		
		point = new Vec3();
		
	}
	
	public Vertex(float x, float y, float z, int index){
		
		point = new Vec3(x, y, z);
		this.index = index;
		
	}
	
	public double get(int i){
		
		switch (i){
		
		case 0: { return point.x; }
	    case 1: { return point.y; }
	    case 2: { return point.z; }
	    
	    default: { throw new ArrayIndexOutOfBoundsException(i); }
	    
		}
		
	}

}
