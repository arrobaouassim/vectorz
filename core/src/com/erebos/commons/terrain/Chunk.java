package com.erebos.commons.terrain;

import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.erebos.math.MathUtils;
import com.erebos.math.Vec3;

public class Chunk {
	
	public static VertexAttributes attributes = MeshBuilder.createAttributes(VertexAttributes.Usage.Position |
			   																 VertexAttributes.Usage.Normal   |
			   																 VertexAttributes.Usage.TextureCoordinates);
	
	private MeshPart part;
	private Mesh mesh;	
	
	public float[] heightdata;
	private float[] vertices;	
	private int[] indices;	
	
	private int width, depth;
	public int vertexResolution;
	
	private float xoffset, zoffset;
	
	public Chunk(int width, int depth, int vertexResolution) {
		
		this.width = width;
		this.depth = depth;
		
		this.vertexResolution = vertexResolution;
				
	}
	
	public void init() {
		
		int numVertices = vertexResolution*vertexResolution;
		
		heightdata = new float[numVertices];
		vertices = new float[numVertices*8];
		indices = new int[(vertexResolution-1)*(vertexResolution-1)*6];
		
//		for(int i = 0; i < numVertices; i++) {
//			
//			heightdata[i] = (float) (Math.random()*.5);
//			
//		}
		
		heightdata[0] = 2;
		
		buildVertices();
		buildIndices();
				
		mesh = new Mesh(true, numVertices, indices.length, attributes);
		mesh.setVertices(vertices);
		mesh.setIndices(indices);
		
		part = new MeshPart(null, mesh, 0, indices.length, GL20.GL_LINES);
	    part.update();
	    						
	}
	
	private void buildVertices() {
		
		int i = 0;
		int j = 0;
		float dx = width/(vertexResolution-1f);
		float dn = 1f/(vertexResolution-1);
		
		Vector3 temp;
				
		for(int row = 0; row < vertexResolution; row++) {
			
			for(int col = 0; col < vertexResolution; col++) {

				j = i/8;
				
				temp = calculateNormalAt(col, row);
				
				vertices[i++] = col*dx;				// x
				vertices[i++] = heightdata[j];		// y
				vertices[i++] = row*dx;				// z
				vertices[i++] = temp.x;					// normal x
				vertices[i++] = temp.y;					// normal y
				vertices[i++] = temp.z;					// normal z
				vertices[i++] = col*dn;				// UV x
				vertices[i++] = 1 - row*dn;			// UV y
				
			}
			
		}		
		
	}
	
	private void buildIndices() {
		
		int i = 0; 
		int ex = 0;
		
		for(int row = 0; row < vertexResolution-1; row++) {
			
			for(int col = 0; col < vertexResolution-1; col++) {
				
				ex = (row*vertexResolution + col);
				
				indices[i++] = (ex + 1);		
				indices[i++] = ex;	
				indices[i++] = (ex + vertexResolution);	
				
				indices[i++] = (ex + vertexResolution);	
				indices[i++] = (ex + vertexResolution + 1);	
				indices[i++] = (ex + 1);	
				
			}
			
		}
		
	}

	private Vector3 calculateNormalAt(int x, int y) {
		
		// handle edges of terrain
	    int xP1 = (x+1 >= vertexResolution) ? vertexResolution -1 : x+1;
	    int yP1 = (y+1 >= vertexResolution) ? vertexResolution -1 : y+1;
	    int xM1 = (x-1 < 0) ? 0 : x-1;
	    int yM1 = (y-1 < 0) ? 0 : y-1;

	    float hL = heightdata[y * vertexResolution + xM1];
	    float hR = heightdata[y * vertexResolution + xP1];
	    float hD = heightdata[yM1 * vertexResolution + x];
	    float hU = heightdata[yP1 * vertexResolution + x];

	    return new Vector3(hL - hR, 2, hD - hU).nor();
	        
	}
		
	//TEMP
    public float getHeightAtWorldCoord(float worldX, float worldZ) {
    	
    	Vec3 c00, c01, c10, c11;
    	
    	c00 = new Vec3(xoffset, 0, zoffset);
    	c11 = new Vec3();
    	
    	float terrainX = worldX - xoffset;
        float terrainZ = worldZ - zoffset;

        float gridSquareSize = width / ((float) vertexResolution - 1);
        int gridX = (int) Math.floor(terrainX / gridSquareSize);
        int gridZ = (int) Math.floor(terrainZ / gridSquareSize);

        if (gridX >= vertexResolution - 1 || gridZ >= vertexResolution - 1 || gridX < 0 || gridZ < 0) {
            return 0;
        }

        float xCoord = (terrainX % gridSquareSize) / gridSquareSize;
        float zCoord = (terrainZ % gridSquareSize) / gridSquareSize;

        c01 = new Vec3(1, heightdata[(gridZ + 1) * vertexResolution + gridX], 0);
        c10 = new Vec3(0, heightdata[gridZ * vertexResolution + gridX + 1], 1);

        // we are in upper left triangle of the square
        if (xCoord <= (1 - zCoord)) {
            c00.set(0, heightdata[gridZ * vertexResolution + gridX], 0);
            return MathUtils.barryCentric(c00, c10, c01, zCoord, xCoord);
        }
        // bottom right triangle
        c11.set(1, heightdata[(gridZ + 1) * vertexResolution + gridX + 1], 1);
        return MathUtils.barryCentric(c10, c11, c01, zCoord, xCoord);
        
    }
	
	public void print() {
		
		for(int i = 0; i < vertices.length; i+=8) {
			
			System.out.println("Vertex " + i/8 + " = (" + vertices[i] + ", " + vertices[i+1] + ", " + vertices[i+2] + ")");
						
		}
		
		for(int i = 0; i < indices.length; i+=3) {
			
			System.out.println("Face " + i/3 + ": " + indices[i] + ", " + indices[i+1] + ", " + indices[i+2]);
			
		}
		
	}
	
	public int getWidth() { return width; }
	
	public MeshPart getMeshPart() { return part; }
	
	public Mesh getMesh() { return part.mesh; }
	
	public void setOffset(float xoffset, float zoffset) {
		
		this.xoffset = xoffset;
		this.zoffset = zoffset;
		
	}
	
}
