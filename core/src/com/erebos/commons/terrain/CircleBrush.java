package com.erebos.commons.terrain;

import com.badlogic.gdx.Gdx;
import com.erebos.core.Scene;

public class CircleBrush extends TerrainBrush {

    public CircleBrush(Terrain terrain, Scene scene) {
        super(terrain, scene.viewport, Gdx.files.internal("assets/circle.png"));
        
        ID = "Circle-Brush";
        
    }
    
}