package com.erebos.commons.terrain;

public class Command {

    private float[] heightDataBefore;
    private float[] heightDataAfter;

    public Terrain terrain;

    public Command(Terrain terrain) {
        this.terrain = terrain;
    }

    public void setHeightDataBefore(float[] data) {
        heightDataBefore = new float[data.length];
        System.arraycopy(data, 0, heightDataBefore, 0, data.length);
    }

    public void setHeightDataAfter(float[] data) {
        heightDataAfter = new float[data.length];
        System.arraycopy(data, 0, heightDataAfter, 0, data.length);
    }

    public void execute() {
        terrain.heightdata = heightDataAfter;
        terrain.update();
    }

    public void undo() {
        terrain.heightdata = heightDataBefore;
        terrain.update();
    }

    public void dispose() {
        heightDataAfter = null;
        heightDataBefore = null;
        terrain = null;
    }
	
}
