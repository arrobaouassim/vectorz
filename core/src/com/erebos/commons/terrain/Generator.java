package com.erebos.commons.terrain;

/**
*
* @author Marcus Brummer
* @version 20-06-2016
*/
public abstract class Generator<T extends Generator<T>> {

   protected Terrain terrain;
   protected float minHeight = 0;
   protected float maxHeight = 50;

   Generator(Terrain terrain) {
       this.terrain = terrain;
   }

   public T minHeight(float min) {
       this.minHeight = min;
       return (T) this;
   }

   public T maxHeight(float max) {
       this.maxHeight = max;
       return (T) this;
   }

   public abstract void terraform();

}
