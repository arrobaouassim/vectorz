package com.erebos.commons.terrain;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.Mesh;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.model.MeshPart;
import com.badlogic.gdx.graphics.g3d.utils.MeshBuilder;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.erebos.core.FileManager;
import com.erebos.core.GameObject;
import com.erebos.math.MathUtils;

public class Terrain extends GameObject{
	
	//Vectors for calculations
    private static final Vector3 c00 = new Vector3();
    private static final Vector3 c01 = new Vector3();
    private static final Vector3 c10 = new Vector3();
    private static final Vector3 c11 = new Vector3();
    	
	//GDX stuff
	private ModelBuilder builder = new ModelBuilder();		
	private Mesh mesh;
	private MeshPart part;
	private Model model;
	
	public static VertexAttributes attributes = MeshBuilder.createAttributes(VertexAttributes.Usage.Position |
				  VertexAttributes.Usage.Normal   |
				  VertexAttributes.Usage.TextureCoordinates);
	
	//Terrain properties
	public int vertexResolution;
	public int size;
	
	//Terrain data
	public float[] heightdata;
	public float[] vertices;
	public int[] indices;
		
	//Terrain instance	
	public ModelInstance instance;
		
	public Terrain(int size, int vertexResolution) {
		
		this.size = size;
		this.vertexResolution = vertexResolution;
				
		init();
		
	}
	
	public Terrain(int size, int vertexResolution, float[] heightdata) {
		
		this.size = size;
		this.vertexResolution = vertexResolution;
		this.heightdata = heightdata;
				
		init();
		
	}
	
	@Override
	public void init() {
		super.init();
		
		int numVertices = vertexResolution*vertexResolution;
		
		if(heightdata == null) heightdata = new float[numVertices];
		vertices = new float[numVertices*8];
		indices = new int[(vertexResolution-1)*(vertexResolution-1)*6];
				
		buildVertices();
		buildIndices();
		buildModel();
		
	}
	
	/* Private for-building-purposes methods */
	
	private void buildVertices() {
		
		int i = 0;
		float dx = size/(vertexResolution-1f);
		float dn = 1f/(vertexResolution-1);
						
		for(int row = 0; row < vertexResolution; row++) {
			
			for(int col = 0; col < vertexResolution; col++) {
				
				c00.set(calculateNormalAt(col, row));
				
				vertices[i++] = col*dx;				// x
				vertices[i++] = heightdata[i/8];	// y
				vertices[i++] = row*dx;				// z
				vertices[i++] = c00.x;				// normal x
				vertices[i++] = c00.y;				// normal y
				vertices[i++] = c00.z;				// normal z
				vertices[i++] = col*dn;				// UV x
				vertices[i++] = 1 - row*dn;			// UV y
				
			}
			
		}		
		
	}
	
	private void buildIndices() {
		
		int i = 0; 
		int ex = 0;
		
		for(int row = 0; row < vertexResolution-1; row++) {
			
			for(int col = 0; col < vertexResolution-1; col++) {
				
				ex = (row*vertexResolution + col);
				
				indices[i++] = ex + 1;		
				indices[i++] = ex;	
				indices[i++] = ex + vertexResolution;	
				
				indices[i++] = ex + vertexResolution;	
				indices[i++] = ex + vertexResolution + 1;	
				indices[i++] = ex + 1;	
				
			}
			
		}
		
	}	
	
	private void buildModel() {
				
		mesh = new Mesh(true, heightdata.length, indices.length, attributes);
		mesh.setVertices(vertices);
		mesh.setIndices(indices);
		
		part = new MeshPart(null, mesh, 0, indices.length, GL20.GL_TRIANGLES);
	    part.update();
		
		builder.begin();
		
		builder.part(part, new Material(ColorAttribute.createDiffuse(Color.GREEN)));        		
        	
        model = builder.end();
		
		instance = new ModelInstance(model);
		
	}
		
	private Vector3 calculateNormalAt(int x, int y) {
		
		// handle edges of terrain
	    int xP1 = (x+1 >= vertexResolution) ? vertexResolution -1 : x+1;
	    int yP1 = (y+1 >= vertexResolution) ? vertexResolution -1 : y+1;
	    int xM1 = (x-1 < 0) ? 0 : x-1;
	    int yM1 = (y-1 < 0) ? 0 : y-1;

	    float hL = heightdata[y * vertexResolution + xM1];
	    float hR = heightdata[y * vertexResolution + xP1];
	    float hD = heightdata[yM1 * vertexResolution + x];
	    float hU = heightdata[yP1 * vertexResolution + x];

	    return new Vector3(hL - hR, 2, hD - hU).nor();
	        
	}
				
	/* Public methods */
	
	//TEMP TODO: OFFSET 
    public float getHeightAtWorldCoord(float worldX, float worldZ) {
    	    	    	
    	float terrainX = worldX;
        float terrainZ = worldZ;

        float gridSquareSize = size / ((float) vertexResolution - 1);
        int gridX = (int) Math.floor(terrainX / gridSquareSize);
        int gridZ = (int) Math.floor(terrainZ / gridSquareSize);

        if (gridX >= vertexResolution - 1 || gridZ >= vertexResolution - 1 || gridX < 0 || gridZ < 0) {
            return 0;
        }

        float xCoord = (terrainX % gridSquareSize) / gridSquareSize;
        float zCoord = (terrainZ % gridSquareSize) / gridSquareSize;

        c01.set(1, heightdata[(gridZ + 1) * vertexResolution + gridX], 0);
        c10.set(0, heightdata[gridZ * vertexResolution + gridX + 1], 1);

        // we are in upper left triangle of the square
        if (xCoord <= (1 - zCoord)) {
            c00.set(0, heightdata[gridZ * vertexResolution + gridX], 0);
            return MathUtils.barryCentric(c00, c10, c01, zCoord, xCoord);
        }
        // bottom right triangle
        c11.set(1, heightdata[(gridZ + 1) * vertexResolution + gridX + 1], 1);
        return MathUtils.barryCentric(c10, c11, c01, zCoord, xCoord);
        
    }
		
    public boolean isUnderTerrain(Vector3 worldCoords) {
    	
    	return getHeightAtWorldCoord(worldCoords.x, worldCoords.z) > worldCoords.y;
    	
    }
    
    public boolean isOnTerrain(float worldX, float worldZ) {
    	
        instance.transform.getTranslation(c00);
        
        return worldX >= c00.x && worldX <= c00.x + size && worldZ >= c00.z && worldZ <= c00.z + size;
                
    }

    public Vector3 getRayIntersection(Vector3 out, Ray ray) {
        // TODO improve performance. use binary search
        float curDistance = 2;
        int rounds = 0;

        ray.getEndPoint(out, curDistance);
        boolean isUnder = isUnderTerrain(out);

        while(true) {
            rounds++;
            ray.getEndPoint(out, curDistance);

            boolean u = isUnderTerrain(out);
            if(u != isUnder || rounds == 10000) {
                return out;
            }
            curDistance += u ? -0.1f : 0.1f;
        }

    }
    
    public Vector3 getVertexPosition(Vector3 out, int x, int z) {
    	
        final float dx = x / (vertexResolution - 1f);
        final float dz = z / (vertexResolution - 1f);        
        final float height = heightdata[z * vertexResolution + x];    
        
        out.set(dx * size, height, dz * size);
        
        return out;
        
    }
    
	public void update() {
				
		buildVertices();
	    mesh.setVertices(vertices);
		
	}
	
    public Vector3 getPosition(Vector3 out) { return instance.transform.getTranslation(out); }
    
    public static void saveTerrain(Terrain terrain, String path) {
    	System.out.println("hola");
    	FileManager.createFile(path + ".terra");
    	FileManager.addInt(terrain.size); 
    	FileManager.addInt(terrain.vertexResolution);
    	for(float h : terrain.heightdata) FileManager.addFloat(h);
    	FileManager.closeFileWriter();
    	
    }
    
    public static Terrain loadTerrain(String path) {
    	    	
		FileManager.openFile(path + ".terra");
		
		int size = FileManager.getNextInt();
    	int res = FileManager.getNextInt();
    	
    	float[] heightdata = new float[res*res];
    	for(int i = 0; i < heightdata.length; i++) {
    		heightdata[i] = FileManager.getNextFloat();
    	}
    	
    	FileManager.closeFileReader();
    	
    	return new Terrain(size, res, heightdata);   	
    	
    }
    
    @Override
    public void dispose() {
    	super.dispose();
    	
    	model.dispose();
    	mesh.dispose();
    	    	    	
    }
    
}
