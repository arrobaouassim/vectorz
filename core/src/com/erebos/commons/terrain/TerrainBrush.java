package com.erebos.commons.terrain;

import com.badlogic.gdx.InputAdapter;
import com.badlogic.gdx.files.FileHandle;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.Pixmap;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.math.collision.Ray;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;

public class TerrainBrush extends InputAdapter{
	
	public String ID;
	public Viewport viewport;
	
	public enum BrushMode { RAISE_LOWER, FLATTEN, SMOOTH, PAINT }
	
	public static enum BrushAction {
		
		PRIMARY(Input.Buttons.LEFT),	
		SECONDARY(Input.Keys.SHIFT_LEFT); 
		
		public final int code;
		
		private BrushAction(int levelCode) { this.code = levelCode; }
		
	}
		
	@SuppressWarnings("serial")
	public class ModeNotSupportedException extends Exception {			
		public ModeNotSupportedException(String message) { super(message); }
	}	

    // Used for calculations
    protected static final Vector2 c = new Vector2();
    protected static final Vector2 p = new Vector2();
    protected static final Vector2 v = new Vector2();
    protected static final Color c0 = new Color();
    protected static final Vector3 tVec0 = new Vector3();
    protected static final Vector3 tVec1 = new Vector3();
    
    // Common Settings
    private static float strength = 5f;
    private static float heightSample = 0f;    
    
    // individual brush settings
    protected final Vector3 brushPos = new Vector3();
    protected BrushMode mode;
    protected Terrain terrain;
    protected float radius = 5;
    private BrushAction action;    
    
    private boolean mouseMoved = false;
        
    // the pixmap brush
    private Pixmap brushPixmap;
    private int pixmapCenter;
    
    // undo/redo system
    private Command command = null;
    private boolean terrainHeightModified = false;
    
    public TerrainBrush(Terrain terrain, Viewport viewport, FileHandle pixmapBrush) {

    	this.terrain = terrain;
    	this.viewport = viewport;
    	
        brushPixmap = new Pixmap(pixmapBrush);
        pixmapCenter = brushPixmap.getWidth() / 2;    	
        
    }
    
    public void act() {
    	
        if(action == null) return;
        if(terrain == null) return;
        
        // sample height
        if(action == BrushAction.SECONDARY && mode == BrushMode.FLATTEN) {
            heightSample = brushPos.y;
            return;
        }

        // only act if mouse has been moved
        if(!mouseMoved) return;
        mouseMoved = false;
        
        if(mode == BrushMode.RAISE_LOWER) {
            raiseLower(action);
        } else if(mode == BrushMode.FLATTEN) {
            flatten();
        }
        
    }
    
    public void flatten(){
    	
        final Vector3 terPos = terrain.getPosition(tVec1);
        
        for (int x = 0; x < terrain.vertexResolution; x++) {
            for (int z = 0; z <  terrain.vertexResolution; z++) {
                final Vector3 vertexPos = terrain.getVertexPosition(tVec0, x, z);
                vertexPos.x += terPos.x;
                vertexPos.z += terPos.z;
                float distance = vertexPos.dst(brushPos);

                if(distance <= radius) {
                    final int index = z * terrain.vertexResolution + x;
                    final float diff = Math.abs(terrain.heightdata[index] - heightSample);
                    if(diff <= 1f) {
                        terrain.heightdata[index] = heightSample;
                    } else if(diff > 1f){
                        final float elevation = getValueOfBrushPixmap(brushPos.x, brushPos.z, vertexPos.x, vertexPos.z, radius);
                        final float newHeight = heightSample * elevation;
                        if(Math.abs(heightSample - newHeight) < Math.abs(heightSample - terrain.heightdata[index])) {
                            terrain.heightdata[index] = newHeight;
                        }
                    }
                }
            }
        }

        terrain.update();
        terrainHeightModified = true;
    	
    }    
    
    private void raiseLower(BrushAction action) {
    	
        final Vector3 terPos = terrain.getPosition(tVec1);        
        float dir = (action == BrushAction.PRIMARY) ? 1 : -1;
        
        for(int x = 0; x < terrain.vertexResolution; x++) {
        	
            for(int z = 0; z < terrain.vertexResolution; z++) {
            	
                final Vector3 vertexPos = terrain.getVertexPosition(tVec0, x, z);
                
                vertexPos.x += terPos.x;
                vertexPos.z += terPos.z;
                float distance = vertexPos.dst(brushPos);
                
                if (distance <= radius) {
                	
                    float elevation = getValueOfBrushPixmap(brushPos.x, brushPos.z, vertexPos.x, vertexPos.z, radius);
                    terrain.heightdata[z * terrain.vertexResolution + x] += dir * elevation * strength;
                    
                }
                
            }
            
        }

        terrain.update();
        terrainHeightModified = true;
        
    }
    
    private float getValueOfBrushPixmap(float centerX, float centerZ, float pointX, float pointZ, float radius) {
    	
        c.set(centerX, centerZ);
        p.set(pointX, pointZ);
        v.set(p.sub(c));

        final float progress = v.len() / radius;
        v.nor().scl(pixmapCenter * progress);

        final float mapX = pixmapCenter + (int) v.x;
        final float mapY = pixmapCenter + (int) v.y;
        c0.set(brushPixmap.getPixel((int) mapX, (int) mapY));

        return c0.r;
    }
	
    public void setMode(BrushMode mode) {
    	
        if (!supportsMode(mode)) {
            try {
				throw new ModeNotSupportedException(ID + " does not support " + mode);
			} catch (ModeNotSupportedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
        }
        this.mode = mode;
    }

    public boolean supportsMode(BrushMode mode) {
    	
        switch(mode) {
        case RAISE_LOWER:
        case FLATTEN:
        case PAINT:
            return true;
		default:
			return false;            
            
        }
        
    }

    public void dispose() {
    	
    	brushPixmap.dispose();
    	
    }
    
    /* Input Processor */
    
    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
    	
        if (terrainHeightModified && command != null) {
        	command.setHeightDataAfter(command.terrain.heightdata);
        }
        
        terrainHeightModified = false;
        
        command = null;        
        action = null;

        return false;
        
    }
    
    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {

        // get action
        final boolean primary = Gdx.input.isButtonPressed(BrushAction.PRIMARY.code);
        final boolean secondary = Gdx.input.isKeyPressed(BrushAction.SECONDARY.code);
        if (primary && secondary) {
            action = BrushAction.SECONDARY;
        } else if (primary) {
            action = BrushAction.PRIMARY;
        } else {
            action = null;
        }

        if (mode == BrushMode.FLATTEN || mode == BrushMode.RAISE_LOWER || mode == BrushMode.SMOOTH) {
            command = new Command(terrain);
            command.setHeightDataBefore(terrain.heightdata);
        }

        return false;
        
    }
    
    @Override
    public boolean mouseMoved(int screenX, int screenY) {
    	
        if (terrain != null) {
        	
            Ray ray = viewport.getPickRay(screenX, screenY);
            terrain.getRayIntersection(brushPos, ray);
            
        }

        mouseMoved = true;

        return false;
        
    }
    
    @Override
    public boolean scrolled(int amount) {
    	
        if (amount < 0) { radius *= 0.9f; } 
        else { radius *= 1.1f; }

        return false;
        
    }
    
    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        return mouseMoved(screenX, screenY);
    }
    
}
