package com.erebos.commons.ui;

public class Button extends UiComponent{

	public Button(String path, String ID) {
		super(path, ID);
		// TODO Auto-generated constructor stub
	}
	
	public Button(String path, String ID, float x, float y, float width, float height) {
		super(path, ID, x, y, width, height);	
		
	}

	@Override
	public void act(int x, int y) {

		System.out.println("BUTTON " + ID + " PRESSED!");
		
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height);
		
		System.out.println(width + ", " + height);
		
	}
	

}
