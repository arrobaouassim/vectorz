package com.erebos.commons.ui;

import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;

public class Menu extends UiComponent{
	
	public Array<UiComponent> components;

	public Menu(String path, String ID) {
		super(path, ID);

		components = new Array<UiComponent>();
		
	}
	
	public Menu(String path, String ID, float x, float y, float width, float height) {
		super(path, ID, x, y, width, height);
		
		components = new Array<UiComponent>();		
		
	}
	
	public void render(SpriteBatch batch) {
		super.render(batch);
		
		for(UiComponent component : components) component.render(batch);		
		
	}

	@Override
	public void act(int x, int y) {
		System.out.println("checking...");
		for(UiComponent component : components) {			
			
			if(component.check(x, y))
				component.act(x, y);
			
		}
		
	}
	
	@Override
	public void resize(int width, int height) {
		super.resize(width, height); 
		
		for(UiComponent component : components) component.resize(width, height); 
		
	}

}
