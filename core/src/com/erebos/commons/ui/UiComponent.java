package com.erebos.commons.ui;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.erebos.core.GameObject;
import com.erebos.math.Vec2;

public abstract class UiComponent extends GameObject{
	
	public Sprite sprite;
	public String ID;
	public Vec2 posLock, sizeLock, posBias, sizeBias;
	
	public UiComponent(String path, String ID) {
		
		sprite = new Sprite(new Texture(Gdx.files.internal(path)));
		this.ID = ID;		
		
		posLock = new Vec2();
		sizeLock = new Vec2();
		posBias = new Vec2();
		sizeBias = new Vec2();
		
	}
	
	public UiComponent(String path, String ID, float x, float y, float width, float height) {
		
		sprite = new Sprite(new Texture(Gdx.files.internal(path)));
		this.ID = ID;		
		
		sprite.setBounds(x, y, width, height); 
		
		posLock = new Vec2();
		sizeLock = new Vec2();
		posBias = new Vec2();	
		sizeBias = new Vec2();

	}
	
	public void render(SpriteBatch batch) {
		
		sprite.draw(batch); 
		
	}
	
	public void setPosLock(float x, float y) {
		
		posLock.set(x, y);
		
	}
	
	public void setSizeLock(float x, float y) {
		
		sizeLock.set(x, y);
		
	}
	
	public void setPosBias(float x, float y) {
		
		posBias.set(x, y); 
		
	}
	
	public void setSizeBias(float x, float y) {
		
		sizeBias.set(x, y); 
		
	}
	
	public boolean check(int x, int y) {
		
//		x -= Gdx.graphics.getWidth()/2;
//		y = y - Gdx.graphics.getHeight()/2;
//		
//		float xmax = sprite.getX() + sprite.getWidth();
//		float ymax = sprite.getY() + sprite.getHeight();
//		float xmin = sprite.getX();
//		float ymin = sprite.getY();
//		
//		if(x > xmax || x < xmin) return false;
//		else if(y > ymax || y < ymin) return false;
//		else return true;
		
		y = Gdx.graphics.getHeight() - y;
		float w = sprite.getWidth();
		float h = sprite.getHeight();
		float px = sprite.getX()+Gdx.graphics.getWidth()/2;
		float py = sprite.getY()+Gdx.graphics.getHeight()/2;
				
		if(x <= px + w && x >= px && y <= py + h && y >= py) return true;
		
		return false;
		
	}
	
	public abstract void act(int x, int y);

	
	//TODO: Improve Bias Correction.
	public void resize(int width, int height) {
		
		if(sizeLock.x == 0 && sizeLock.y == 0) {
			
		}else if(sizeLock.x != 0 && sizeLock.y != 0) {
			
			sprite.setSize(width*sizeLock.x+sizeBias.x, height*sizeLock.y+sizeBias.y);
			
		}else if(sizeLock.x != 0) {
			
			sprite.setSize(width*sizeLock.x+sizeBias.x, sprite.getHeight()+sizeBias.y);
			
		}else {
			
			sprite.setSize(sprite.getWidth()+sizeBias.x, height*sizeLock.y+sizeBias.y);
			
		}
		
		
		if(posLock.x == 0 && posLock.y == 0) {
			
		}else if(posLock.x != 0 && posLock.y != 0) {
			
			sprite.setPosition(width*posLock.x+posBias.x, height*posLock.y+posBias.y);
			return;
			
		}else if(posLock.x != 0) {
			
			sprite.setPosition(width*posLock.x+posBias.x, sprite.getY()+posBias.y);
			
		}else {
			
			sprite.setPosition(sprite.getX()+posBias.x, height*posLock.y+posBias.y);
			
		}
		
	}
	
}
