package com.erebos.commons.ui;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.ScreenViewport;
import com.badlogic.gdx.utils.viewport.Viewport;

public abstract class UiInterface{
	
	public String ID;
	public Array<UiComponent> components;
	public BitmapFont font;
	public SpriteBatch batch;
	public Viewport viewport;
	public Camera orthocam;
	public int currentWidth, currentHeight;
	
	public UiInterface(String ID) {
		
		this.ID = ID;
		components = new Array<UiComponent>();
		font = new BitmapFont();
		batch = new SpriteBatch();
				
		orthocam = new OrthographicCamera();
		viewport = new ScreenViewport(orthocam);
		
	}
	
	public abstract void init();
	
	public void input(int x, int y) {
		
		for(UiComponent component : components) {			
		
			if(component.check(x, y))
				component.act(x, y);
			
		}
		
	}
	
	public void render() {
		
		viewport.apply();
		batch.setProjectionMatrix(orthocam.combined);
		batch.begin();
		for(UiComponent component : components) component.render(batch);
		batch.end();
		
	}
	
	public void resize(int width, int height) {
		
		viewport.update(width, height); 
				
		for(UiComponent component : components) component.resize(width, height); 
		
	}
	
	public void dispose() {
		
		for(UiComponent component : components) component.sprite.getTexture().dispose();
		batch.dispose();
		font.dispose();
		components.clear();
		
	}


}
