package com.erebos.core;

import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

public abstract class Display implements Screen{
	
	protected Main game;
	protected ModelBatch batch;
	protected Root root;
	
	public String ID;
	
	public Display(Main game){ 
		
		this.game = game; 
		
		batch = new ModelBatch();
		root = new Root(this, "root");
		
	}
	
	@Override
	public void pause() {}

	@Override
	public void resume() {}

	@Override
	public void hide() {}

	@Override
	public void dispose() {
		
		batch.dispose();
		root.dispose();

	}

}
