package com.erebos.core;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;

public class FileManager {
	
	public static FileWriter writer;
	public static FileReader reader;
	public static BufferedReader buffer;
		
	public static void createFile(String path) {
						
		try {
			writer = new FileWriter(path);			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
			
	}
	
	public static void openFile(String path) {
		
		try {
			reader = new FileReader(path);
			buffer = new BufferedReader(reader);
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	
	
	public static void addString(String string) {
		
		try {
			writer.write(string +"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
	}
	
	public static void addInt(int value){
		
		try {
			writer.write(String.valueOf(value) +"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}
	
	public static void addFloat(float height){
		
		try {
			writer.write(String.valueOf(height) +"\n");
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} 
		
		
	}
	
	public static int getNextInt() {
		
		try {
			return Integer.valueOf(buffer.readLine());
		} catch (IOException e) {
			e.printStackTrace();
			
			return -1;
		}		
		
	}
	
	public static float getNextFloat() {
				
		try {
			return Float.valueOf(buffer.readLine());
		} catch (IOException e) {
			e.printStackTrace();
			
			return -1;
		}		
		
	}
	
	public static String getNextString() {
		
		try {
			return buffer.readLine();
		} catch (IOException e) {
			e.printStackTrace();
			
			return null;
		}		
		
	}
	
	public static void closeFileWriter() {
		
		try {
			writer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public static void closeFileReader() {
		
		try {
			buffer.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
}
