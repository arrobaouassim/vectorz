package com.erebos.core;

public interface GameComponent {
	
	public void init();
	public void update(float delta);
	public void render();
	public void resize(int width, int height);
	public void dispose();

}
