package com.erebos.core;

import com.badlogic.gdx.utils.Array;

public class GameObject {

	public Array<GameObject> children;
	public Array<GameComponent> components;
	public String ID;
	
	public GameObject(){
		
		children = new Array<GameObject>();
		components = new Array<GameComponent>();
		
	}
	
	public GameObject(String ID){
		
		children = new Array<GameObject>();
		components = new Array<GameComponent>();
		this.ID = ID;
		
	}
		
	public void init(){
		
		for(GameObject child : children)
			child.init();
		
		for(GameComponent component : components)
			component.init();
		
	}
		
	public void update(float delta){
		
		for(GameObject child : children)
			child.update(delta); 
		
		for(GameComponent component : components)
			component.update(delta);
		
	}
	
	public void resize(int width, int height){
		
		for(GameObject child : children)
			child.resize(width, height); 
		
		for(GameComponent component : components)
			component.resize(width, height);
		
	}
		
	public void dispose(){
		
		for(GameObject child : children)
			child.dispose(); 
		
		for(GameComponent component : components)
			component.dispose();
		
		components.clear();
		children.clear();
		
	}
		
	public GameObject addChild(GameObject child) { 
		
		children.add(child);
		
		return this;
		
	}
	
	public GameObject addChild(GameObject... child) {
		
		children.addAll(child);
		
		return this;
		
	}
	
	public GameObject addComponent(GameComponent component) { 
		
		components.add(component);
		
		return this;
		
	}
	
	public GameObject addComponent(GameComponent... component) { 
		
		components.addAll(component);
		
		return this;
		
	}
	
}
