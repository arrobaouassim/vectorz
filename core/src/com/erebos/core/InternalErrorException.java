package com.erebos.core;

@SuppressWarnings("serial")
public class InternalErrorException extends RuntimeException{
	public InternalErrorException (String msg) { super(msg); }
}
