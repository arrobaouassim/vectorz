package com.erebos.core;

import com.badlogic.gdx.Game;
import com.erebos.editor.Editor;

public class Main extends Game {
	
	public Editor editor;
	
	@Override
	public void create () {
		
		editor = new Editor(this);
		
		setScreen(editor);

	}
	
}
