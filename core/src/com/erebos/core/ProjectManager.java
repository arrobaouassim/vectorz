package com.erebos.core;

import java.io.File;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.utils.Array;
import com.erebos.commons.terrain.Terrain;
import com.erebos.commons.ui.UiInterface;
import com.erebos.editor.Modelbuilder;

public class ProjectManager {
	
	public Display currentDisplay;
	public Scene currentScene;
	public Array<Scene> scenes;
	
	public ProjectManager() {
		
		scenes = new Array<Scene>();
		
	}
	
	public void saveProject() {
		
		
	}
	
	public void loadProject() {
		
		
	}
	
	public Scene setActiveScene(String ID) {
		
		for(Scene scene : scenes) 
			if(scene.ID == ID) currentScene = scene;
				
		currentDisplay = currentScene.root.display;
		
		return currentScene;
		
	}
	
	public Scene newScene(Display display, UiInterface gui, String ID) {
		
		scenes.add(new Scene(display, gui, ID));
		
		return scenes.peek();		
		
	}
		
	public void saveScene() {
		
		Vector3 temp = new Vector3();
		String path = "assets/" + currentDisplay.ID + "/Scenes/" + currentScene.ID + "/";
		
		
		File dir = new File(path + "Terrains/");
		if(!dir.exists()) dir.mkdirs();
		
		//assets\display\scenes\scene\terrains\scene.terra
		Terrain.saveTerrain(currentScene.terrain, path + "Terrains/" + currentScene.ID);		
		
		//assets\display\scenes\scene\scene.scn
		FileManager.createFile(path + currentScene.ID + ".scn");						
		
		FileManager.addInt(currentScene.renderableObjects.size);
		for(RenderableObject object : currentScene.renderableObjects) {
			//TODO: Must save object model (.fbx, .g3db, ...) instead of Object ID. *ONLY for testing purposes.*
			FileManager.addString(object.ID);
			FileManager.addFloat(object.instance.transform.getTranslation(temp).x);
			FileManager.addFloat(temp.y);
			FileManager.addFloat(temp.z);
			FileManager.addFloat(object.instance.transform.getScaleX());
			FileManager.addFloat(object.instance.transform.getScaleY());
			FileManager.addFloat(object.instance.transform.getScaleZ());
			/* ROTATION NEEDED */						
		}
		
		FileManager.closeFileWriter();
				
	}
	
	public Scene loadScene(UiInterface gui, String ID) {
		
		Model temp = Modelbuilder.createBox(1, 1, 1, Color.BLACK);
		
		String path = "assets/" + currentDisplay.ID + "/Scenes/" + ID + "/";
		
		Scene scene = new Scene(currentDisplay, gui, ID);
		
		FileManager.openFile(path + ID + ".scn");
		int size = FileManager.getNextInt();
		for(int i = 0; i < size; i++) {
			
			scene.addRenderableObject(new RenderableObject(FileManager.getNextString(), temp))
			.instance.transform
			.setToTranslationAndScaling(FileManager.getNextFloat(), FileManager.getNextFloat(), FileManager.getNextFloat(), 
										FileManager.getNextFloat(), FileManager.getNextFloat(), FileManager.getNextFloat());
						
		}
		FileManager.closeFileReader();
		
		scene.terrain = Terrain.loadTerrain(path + "Terrains/" + ID);
		scene.root.addChild(scene.terrain, scene.terrain.instance);
				
		scenes.add(scene); 
		
		return scene;
		
	}

}
