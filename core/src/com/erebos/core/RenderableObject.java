package com.erebos.core;

import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;

public class RenderableObject extends GameObject{
	
	public ModelInstance instance;
	
	public RenderableObject(String ID, Model model) {
		super(ID);
		
		instance = new ModelInstance(model);
		
	}

}
