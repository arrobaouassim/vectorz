package com.erebos.core;

import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.utils.Array;

public class Root extends GameObject{
	
	public Display display;	
	public Array<ModelInstance> instances;
	
	public Root(Display display, String ID) {		
		super(ID);
		
		this.display = display;
		instances = new Array<ModelInstance>();
		
	}
	
	public void update(Camera cam, Environment environment, float delta) {
		super.update(delta);
		
		display.batch.begin(cam);		
		display.batch.render(instances, environment);
		display.batch.end();
				
	}
	
	public Root addChild(GameObject child, ModelInstance instance) {
		super.addChild(child);
		
		instances.add(instance); 
		
		return this;
		
	}

}
