package com.erebos.core;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.utils.Array;
import com.badlogic.gdx.utils.viewport.FillViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.erebos.commons.terrain.Terrain;
import com.erebos.commons.ui.UiInterface;

public class Scene {	
	
	public String ID;
		
	public Root root;
	
	public UiInterface gui;
	
	public Terrain terrain;
	public Array<RenderableObject> renderableObjects;
	public Array<GameObject> objects;
	
	public Viewport viewport;
	public PerspectiveCamera camera;
	
	public Environment environment;
	
	public Scene(Display display, UiInterface gui, String ID) {
		
		this.ID = ID;
		this.gui = gui;
		
		root = new Root(display, "root");
		
		init();
		
	}
	
	public void init() {
		
		//Environment
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		
		gui.init();
		
		renderableObjects = new Array<RenderableObject>();
		objects = new Array<GameObject>();
		
        camera = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        camera.position.set(0, 0, 10);
        camera.lookAt(0, 0, 0);
        camera.near = 1f;
        camera.far = 300f;
        camera.update();
        
        viewport = new FillViewport(Gdx.graphics.getWidth(), Gdx.graphics.getHeight(), camera);
		
		root.init();  
		
	}
	
	public void update(float delta) {
		
		gui.render();
		root.update(camera, environment, delta);
		
	}
	
	public void resize(int width, int height) {
		
		gui.resize(width, height);
		viewport.update(width, height); 
		root.resize(width, height); 
		
	}
	
	public void dispose() {
		
		gui.dispose();
		root.dispose();
		renderableObjects.clear();
		objects.clear();
		
	}
	
	public RenderableObject	addRenderableObject(RenderableObject renderableObject) {
		
		renderableObjects.add(renderableObject); 
		root.addChild(renderableObject, renderableObject.instance);
		
		return renderableObject;
		
	}
	
	public Scene addRenderableObject(RenderableObject ... renderableObject) {
		
		
		for(RenderableObject object : renderableObject) {
			
			renderableObjects.add(object);
			root.addChild(object, object.instance);
			
		}
		
		return this;
		
	}
	

}
