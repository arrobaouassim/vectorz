package com.erebos.editor;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.InputMultiplexer;
import com.badlogic.gdx.graphics.GL20;
import com.erebos.commons.terrain.CircleBrush;
import com.erebos.commons.terrain.TerrainBrush.BrushMode;
import com.erebos.core.Display;
import com.erebos.core.Main;
import com.erebos.core.ProjectManager;

public class Editor extends Display{
	
	public InputMultiplexer multiplexer;
	public FreeCamController controller;
	public CircleBrush brush;
	public ProjectManager manager;

	public Editor(Main game) {
		super(game);	
		
		this.ID = "Editor";
	}

	@Override
	public void show() {
				                                
        manager = new ProjectManager();        
        manager.currentDisplay = this;
        manager.loadScene(new EditorUi("TESTING"), "TestScene");
             
        //Input
        multiplexer = new InputMultiplexer();        
        controller = new FreeCamController(manager.scenes.first().camera);         
        
        multiplexer.addProcessor(controller);
                
        Gdx.input.setInputProcessor(multiplexer);
                
        brush = new CircleBrush(manager.scenes.first().terrain, manager.scenes.first());
        brush.setMode(BrushMode.RAISE_LOWER);
                                              
        multiplexer.addProcessor(brush); 
                                
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(.1f, 0, .2f, 1); 
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
						
		controller.update(); 
		brush.act();
				
		manager.scenes.first().update(delta);				
		        
	}
		
	@Override
	public void resize(int width, int height) {
				
		manager.scenes.first().resize(width, height); 		
				
	}

}
