package com.erebos.editor;

import com.erebos.commons.ui.Button;
import com.erebos.commons.ui.Menu;
import com.erebos.commons.ui.UiInterface;

public class EditorUi extends UiInterface{

	public EditorUi(String ID) {
		super(ID);
		// TODO Auto-generated constructor stub
	}

	@Override
	public void init() {

		Menu menu = new Menu("assets/menu.png", "Menu", 0, 0, 100, 0);

		menu.setPosLock(-0.5f, -0.5f);
		menu.setSizeLock(0, 1); 
		menu.setPosBias(0, 100);
		menu.setSizeBias(0, -100);
		
		Button button = new Button("assets/badlogic.jpg", "Start", 0, 0, 50, 20);
		
		button.setPosLock(-0.5f, 0.5f);
		button.setSizeLock(0, 0); 
		button.setPosBias(20, -100);
		button.setSizeBias(0, 0);
		
		menu.components.add(button); 
		
		Menu menu2 = new Menu("assets/badlogic.jpg", "Menu", 0, 0, 0, 100);

		menu2.setPosLock(-0.5f, -0.5f);
		menu2.setSizeLock(1, 0); 
		
		Menu menu3 = new Menu("assets/menu.png", "Menu", 0, 0, 100, 0);

		menu3.setPosLock(0.5f, -0.5f);
		menu3.setSizeLock(0, 1); 
		menu3.setPosBias(-100, 100);
		menu3.setSizeBias(0, -100);
		
		components.add(menu, menu2, menu3); 
		
	}

}
