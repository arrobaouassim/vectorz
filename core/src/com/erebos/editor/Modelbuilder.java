package com.erebos.editor;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;
import com.erebos.math.Vec3;

public class Modelbuilder {

	private static ModelBuilder modelBuilder = new ModelBuilder();
	
	public static Model createSphere(float radius, Color color){
		
		return modelBuilder.createSphere(radius*2, radius*2, radius*2, 100, 100,
				new Material(ColorAttribute.createDiffuse(color)), 
				VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
		
	}	
	public static Model createBox(float width, float height, float depth, Color color){
		
		return modelBuilder.createBox(width, height, depth, 
				new Material(ColorAttribute.createDiffuse(color)), 
				VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
		
	}
	
	public static Model createCilynder(float width, float height, Color color){
				
		return modelBuilder.createCylinder(width, height, width, 100, 
				new Material(ColorAttribute.createDiffuse(color)), 
				VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
		
	}
	public static Model createCone(float width, float height, float depth, Color color){
		
		return modelBuilder.createCone(width, height, depth, 3, 
				new Material(ColorAttribute.createDiffuse(color)),
				VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
	}
	public static Model createArrow(Vec3 origin, Vec3 end){
		
		return modelBuilder.createArrow(origin.x, origin.y, origin.z, end.x, end.y, end.z, .3f, .2f, 10, GL20.GL_TRIANGLES, 
				new Material(ColorAttribute.createDiffuse(Color.GREEN)), 
				VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
		
	}
	public static Model createArrow(Vec3 origin, Vec3 end, Color color){
		
		return modelBuilder.createArrow(origin.x, origin.y, origin.z, end.x, end.y, end.z, .3f, .2f, 10, GL20.GL_TRIANGLES, 
				new Material(ColorAttribute.createDiffuse(color)), 
				VertexAttributes.Usage.Position|VertexAttributes.Usage.Normal);
		
	}
	
}