package com.erebos.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input.Keys;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.PerspectiveCamera;
import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.environment.DirectionalLight;
import com.badlogic.gdx.graphics.g3d.utils.FirstPersonCameraController;
import com.badlogic.gdx.math.Vector3;
import com.erebos.commons.terrain.Terrain;
import com.erebos.core.Display;
import com.erebos.core.Main;

public class Test extends Display{

	//Just for testing purposes.
	
	private Environment environment;
	private PerspectiveCamera cam;
	private FirstPersonCameraController controller;
	
	private Terrain terrain;
	
	private Vector3 temp;
	private float y, yvel;
	
	private boolean spaced = false;
	
	public Test(Main game) {
		super(game);	
	}

	@Override
	public void show() {
		
		//Environment
        environment = new Environment();
        environment.set(new ColorAttribute(ColorAttribute.AmbientLight, 0.4f, 0.4f, 0.4f, 1f));
        environment.add(new DirectionalLight().set(0.8f, 0.8f, 0.8f, -1f, -0.8f, -0.2f));
		
		//Camera
        cam = new PerspectiveCamera(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        cam.position.set(100, 50, 100);
        cam.near = 1f;
        cam.far = 300f;
        cam.update();
        
        controller = new FirstPersonCameraController(cam);
        Gdx.input.setInputProcessor(controller);
        
        terrain = new Terrain(200, 550); 
        root.addChild(terrain, terrain.instance);
                    		
        temp = new Vector3();
                
	}

	@Override
	public void render(float delta) {

		Gdx.gl.glClearColor(.1f, 0, .2f, 1); 
		Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);

		y = terrain.getHeightAtWorldCoord(cam.position.x, cam.position.z) + 2;
				
		input(delta);
				
		if(colliding() && !spaced) {
			
			yvel = -(cam.position.y - y) * 2;
			
		}else {
			
        	yvel -= 9.8 * delta;
        				
		}
		
        if(Gdx.input.isKeyPressed(Keys.SPACE) || Gdx.input.isKeyJustPressed(Keys.SPACE)) {
        	
        	if(colliding()) {
        		
        		cam.position.y = y;
        		
        		yvel = 7;
        		spaced = true;
        		
        	}        	  
      	  
        }else {
        	
        	spaced = false;
        	
        }
		
		cam.position.y += yvel * delta;
		
		cam.update();
		                
        root.update(cam, environment, delta); 
		        
	}
	
	public void input(float delta) {
		
		float degreesPerPixel = 0.5f;
		float vel = 5;
		
		if(Gdx.input.isTouched()){
			
			float deltaX = -Gdx.input.getDeltaX() * degreesPerPixel;
			float deltaY = -Gdx.input.getDeltaY() * degreesPerPixel;
			cam.direction.rotate(cam.up, deltaX);
			temp.set(cam.direction).crs(cam.up).nor();
			cam.direction.rotate(temp, deltaY);

		}

		
		if(Gdx.input.isKeyPressed(Keys.W)) {
			
			temp.set(cam.direction).nor().scl(delta * vel);
			cam.position.add(temp.x, 0, temp.z);
						
		}
		
		if(Gdx.input.isKeyPressed(Keys.S)) {
			
			temp.set(cam.direction).nor().scl(delta * vel);
			cam.position.add(-temp.x, 0, -temp.z);
						
		}
		
		if(Gdx.input.isKeyPressed(Keys.A)) {
			
			temp.set(cam.direction).crs(cam.up).nor().scl(-delta * vel);
			cam.position.add(temp.x, 0, temp.z);
						
		}
		
		if(Gdx.input.isKeyPressed(Keys.D)) {
			
			temp.set(cam.direction).crs(cam.up).nor().scl(delta * vel);
			cam.position.add(temp.x, 0, temp.z);
						
		}
		
	}

	public boolean colliding() {
		
		return cam.position.y <= y;
		
	}
	
	@Override
	public void resize(int width, int height) {

		cam.viewportWidth = width;
		cam.viewportHeight = height;
		
		cam.update();
		
	}

}
