package com.erebos.math;

import com.badlogic.gdx.math.Vector3;

public class MathUtils {
	
	public static float barryCentric(Vec3 p1, Vec3 p2, Vec3 p3, float x, float y) {		
		
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        float l1 = ((p2.z - p3.z) * (x - p3.x) + (p3.x - p2.x) * (y - p3.z)) / det;
        float l2 = ((p3.z - p1.z) * (x - p3.x) + (p1.x - p3.x) * (y - p3.z)) / det;
        float l3 = 1.0f - l1 - l2;
        
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;		
		
	}
	
	public static float barryCentric(Vector3 p1, Vector3 p2, Vector3 p3, float x, float y) {		
		
		float det = (p2.z - p3.z) * (p1.x - p3.x) + (p3.x - p2.x) * (p1.z - p3.z);
        float l1 = ((p2.z - p3.z) * (x - p3.x) + (p3.x - p2.x) * (y - p3.z)) / det;
        float l2 = ((p3.z - p1.z) * (x - p3.x) + (p1.x - p3.x) * (y - p3.z)) / det;
        float l3 = 1.0f - l1 - l2;
        
        return l1 * p1.y + l2 * p2.y + l3 * p3.y;		
		
	}
	

}
