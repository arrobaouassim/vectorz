package com.erebos.math;

import com.badlogic.gdx.math.Vector3;

public class Vec2 {

	public float x, y;
	
	public Vec2(){
		
		x = 0;
		y = 0;
		
	}
	
	public Vec2(float x, float y){
		
		this.x = x;
		this.y = y;
				
	}
	
	public Vec2(Vec2 vec){
		
		this.x = vec.x;
		this.y = vec.y;
		
	}
		
	// Methods
	
	public Vec2 set(float x, float y){
		
		this.x = x;
		this.y = y;
		
		return this;
		
	}
	
	public Vec2 set(Vec2 vec){
		
		set(vec.x, vec.y);
		
		return this;
		
	}
	
	public Vec2 set(Vector3 vec) {
		
		set(vec.x, vec.y);
		
		return this;
		
	}
	
	public Vec2 setZero(){
		
		set(0, 0);
		
		return this;
		
	}
	
	public Vec2 add(float x, float y){
		
		set(this.x + x, this.y + y);
		
		return this;
		
	}
	
	public Vec2 add(Vec2 vec){
		
		set(x + vec.x, y + vec.y);
		
		return this;
		
	}
	
	public Vec2 sub(float x, float y, float z){
		
		set(this.x - x, this.y - y);
		
		return this;
		
	}
	
	public Vec2 sub(Vec2 vec){
		
		set(x - vec.x, y - vec.y);
		
		return this;
		
	}
	
	public Vec2 scale(float s){
		
		set(x*s, y*s);
		
		return this;
		
	}
	
	public Vec2 normalize(){
		
		set(x/length(), y/length());
		
		return this;
		
	}
		
	
	// Static Operators
	
	public static Vec2 sum(Vec2 vec1, Vec2 vec2){
		
		Vec2 vector = new Vec2(vec1);
		
		return vector.add(vec2);
		
	}
	
	public static Vec2 sub(Vec2 vec1, Vec2 vec2){
		
		Vec2 vector = new Vec2(vec1);
		
		return vector.sub(vec2);
		
	}
	
	public static float dot(Vec2 vec1, Vec2 vec2){
		
		float dot = vec1.x*vec2.x + vec1.y*vec2.y;
		
		return dot;		
		
	}
	
	public static float cross(Vec2 vec1, Vec2 vec2){
				
		return vec1.x*vec2.y - vec2.x*vec1.y;		
		
	}	
	
	public static Vec2 normalize(Vec2 vec){
		
		Vec2 vector = new Vec2(vec);
		
		return vector.normalize();
		
	}
	
	public static float distance(Vec2 vec1, Vec2 vec2){
				
		return Vec2.sub(vec1, vec2).length();
		
	}
	
	public static float distanceSquared(Vec2 vec1, Vec2 vec2){
		
		return Vec2.sub(vec1, vec2).lengthSquared();
		
	}
	
	public static float distanceSquared(float x1, float y1, float z1, float x2, float y2, float z2) {
		
		return lenghtSquared(x2-x1, y2-y1, z2-z1);
		
	}
	
	// Functions
	
	public void draw(){
		
		System.out.println("(x,y,z) = (" + x + ", " + y + ")");
		
	}
	
	public String getString(){
		
		return "(x,y,z) = (" + x + ", " + y + ")";
		
	}
	
	public float length(){
		
		return (float) Math.sqrt(x*x + y*y);
		
	}
	
	public float lengthSquared(){
		
		return x*x + y*y;
		
	}
	
	public static float lenghtSquared(float x, float y, float z) {
		
		return x*x + y*y + z*z;
		
	}
	
	public boolean isZero(){
		
		return lengthSquared() == 0;
		
	}
	
	public boolean equals(Vec2 vec){
		
		return x == vec.x && y == vec.y;
		
	}
	
	public boolean isParallel(Vec2 vec){
		
		return Vec2.normalize(this).equals(Vec2.normalize(vec));
		
	}
	
	public boolean isPerpendicular(Vec2 vec){
		
		return Vec2.dot(this, vec) == 0;
		
	}
	
}
