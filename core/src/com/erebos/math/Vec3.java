package com.erebos.math;

import com.badlogic.gdx.math.Vector3;

public class Vec3 {

	public float x, y, z;
	
	public Vec3(){
		
		x = 0;
		y = 0;
		z = 0;
		
	}
	
	public Vec3(float x, float y, float z){
		
		this.x = x;
		this.y = y;
		this.z = z;
				
	}
	
	public Vec3(Vec3 vec){
		
		this.x = vec.x;
		this.y = vec.y;
		this.z = vec.z;
		
	}
		
	// Methods
	
	public Vec3 set(float x, float y, float z){
		
		this.x = x;
		this.y = y;
		this.z = z;
		
		return this;
		
	}
	
	public Vec3 set(Vec3 vec){
		
		set(vec.x, vec.y, vec.z);
		
		return this;
		
	}
	
	public Vec3 set(Vector3 vec) {
		
		set(vec.x, vec.y, vec.z);
		
		return this;
		
	}
	
	public Vec3 setZero(){
		
		set(0, 0, 0);
		
		return this;
		
	}
	
	public Vec3 add(float x, float y, float z){
		
		set(this.x + x, this.y + y, this.z + z );
		
		return this;
		
	}
	
	public Vec3 add(Vec3 vec){
		
		set(x + vec.x, y + vec.y, z + vec.z);
		
		return this;
		
	}
	
	public Vec3 sub(float x, float y, float z){
		
		set(this.x - x, this.y - y, this.z - z );
		
		return this;
		
	}
	
	public Vec3 sub(Vec3 vec){
		
		set(x - vec.x, y - vec.y, z - vec.z);
		
		return this;
		
	}
	
	public Vec3 scale(float s){
		
		set(x*s, y*s, z*s);
		
		return this;
		
	}
	
	public Vec3 normalize(){
		
		set(x/length(), y/length(), z/length());
		
		return this;
		
	}
		
	
	// Static Operators
	
	public static Vec3 sum(Vec3 vec1, Vec3 vec2){
		
		Vec3 vector = new Vec3(vec1);
		
		return vector.add(vec2);
		
	}
	
	public static Vec3 sub(Vec3 vec1, Vec3 vec2){
		
		Vec3 vector = new Vec3(vec1);
		
		return vector.sub(vec2);
		
	}
	
	public static float dot(Vec3 vec1, Vec3 vec2){
		
		float dot = vec1.x*vec2.x + vec1.y*vec2.y + vec1.z*vec2.z;
		
		return dot;		
		
	}
	
	public static Vec3 cross(Vec3 vec1, Vec3 vec2){
		
		float x = vec1.y*vec2.z - vec2.y*vec1.z;
		float y = -(vec1.x*vec2.z - vec2.x*vec1.z);
		float z = vec1.x*vec2.y - vec2.x*vec1.y;
		
		return new Vec3(x, y, z);		
		
	}	
	
	public static Vec3 normalize(Vec3 vec){
		
		Vec3 vector = new Vec3(vec);
		
		return vector.normalize();
		
	}
	
	public static float distance(Vec3 vec1, Vec3 vec2){
				
		return Vec3.sub(vec1, vec2).length();
		
	}
	
	public static float distanceSquared(Vec3 vec1, Vec3 vec2){
		
		return Vec3.sub(vec1, vec2).lengthSquared();
		
	}
	
	public static float distanceSquared(float x1, float y1, float z1, float x2, float y2, float z2) {
		
		return lenghtSquared(x2-x1, y2-y1, z2-z1);
		
	}
	
	// Functions
	
	public void draw(){
		
		System.out.println("(x,y,z) = (" + x + ", " + y + ", " + z + ")");
		
	}
	
	public String getString(){
		
		return "(x,y,z) = (" + x + ", " + y + ", " + z + ")";
		
	}
	
	public float length(){
		
		return (float) Math.sqrt(x*x + y*y + z*z);
		
	}
	
	public float lengthSquared(){
		
		return x*x + y*y + z*z;
		
	}
	
	public static float lenghtSquared(float x, float y, float z) {
		
		return x*x + y*y + z*z;
		
	}
	
	public boolean isZero(){
		
		return lengthSquared() == 0;
		
	}
	
	public boolean equals(Vec3 vec){
		
		return x == vec.x && y == vec.y && z == vec.z;
		
	}
	
	public boolean isParallel(Vec3 vec){
		
		return Vec3.normalize(this).equals(Vec3.normalize(vec));
		
	}
	
	public boolean isPerpendicular(Vec3 vec){
		
		return Vec3.dot(this, vec) == 0;
		
	}
	
}
