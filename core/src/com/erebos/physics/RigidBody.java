package com.erebos.physics;

import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.erebos.core.GameObject;
import com.erebos.math.Vec3;

public class RigidBody extends GameObject{

	public Vec3 position, velocity, force;
	public Vec3 orient, angularVelocity, torque;
	public float mass, invMass, inertia, invInertia, restitution;
	public float staticFriction, dynamicFriction;
	public Shape shape;

	public RigidBody(){
		
		position = new Vec3();
		velocity = new Vec3();
		force = new Vec3();
		
		orient = new Vec3();
		angularVelocity = new Vec3();
		torque = new Vec3();
		
		mass = 1;
		invMass = 1;
		inertia = 1;
		invInertia = 1;
		restitution = .5f;
		staticFriction = .5f;
		dynamicFriction = .5f;
				
	}
	
	public RigidBody(float x, float y, float z){
		
		position = new Vec3(x, y, z);
		velocity = new Vec3();
		force = new Vec3();
		
		orient = new Vec3();
		angularVelocity = new Vec3();
		torque = new Vec3();
		
		mass = 1;
		invMass = 1;
		inertia = 1;
		invInertia = 1;
		restitution = .5f;
		staticFriction = .5f;
		dynamicFriction = .5f;
		
	}
	
	public RigidBody(Shape shape){
		
		this.shape = shape;
		
		position = new Vec3();
		velocity = new Vec3();
		force = new Vec3();
		
		orient = new Vec3();
		angularVelocity = new Vec3();
		torque = new Vec3();
		
		mass = 1;
		invMass = 1;
		inertia = 1;
		invInertia = 1;
		restitution = .5f;
		staticFriction = .5f;
		dynamicFriction = .5f;
		
	}
	
	// Methods
	
	public RigidBody set(Vec3 position, Vec3 velocity, Vec3 orient, Vec3 angularVelocity){
		
		this.position = position;
		this.velocity = velocity;
		this.orient = orient;
		this.angularVelocity = angularVelocity;		
			
		return this;
		
	}
	
	public RigidBody set(Shape shape){
		
		this.shape = shape;
		
		return this;
		
	}
	
	public RigidBody setStatic(){
		
		setMass(0);
		setInertia(0);
		
		return this;
		
	}
	
	public void setMass(float mass){
		
		this.mass = mass;
		
		if(mass == 0)
			invMass = 0;
		else
			invMass = 1/mass;
		
	}
	
	public void setInertia(float inertia){
		
		this.inertia = inertia;
		
		if(inertia == 0)
			invInertia = 0;
		else
			invInertia = 1/inertia;
		
	}
	
	public void render(ModelBatch batch){
				
	}
	
	@Override
	public void update(float delta){
		
		shape.update(position, orient);
		
	}
	
}
