package com.erebos.physics;

import com.badlogic.gdx.graphics.g3d.Environment;
import com.badlogic.gdx.graphics.g3d.ModelBatch;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.Matrix4;
import com.erebos.math.Vec3;

public abstract class Shape {
	
	public enum Type { Poly }
	
	public RigidBody body;
	public ModelInstance instance;
	public Matrix4 transform ;
	
	public void init(){
	
		transform = new Matrix4();
				
	}
	
	public void update(Vec3 position, Vec3 orient){
		
		transform.setFromEulerAnglesRad(orient.x, orient.y, orient.z)
		.setTranslation(position.x, position.y, position.z);
		
		if(instance != null) instance.transform.set(transform);
		
	}
	
	public void render(ModelBatch batch, Environment environment){
		
		if(instance != null) batch.render(instance, environment);
		
	}

}
